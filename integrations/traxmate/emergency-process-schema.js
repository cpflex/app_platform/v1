"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    case "Emergency.Mode": {
      if (orgData.svals["0"] === "Emergency.Mode.Enable") {
        const notificationSetting = {
          body: _device.name + " SOS alarm activated",
          // eslint-disable-next-line max-len
          icon: "https://s3.eu-west-1.amazonaws.com/v3.thinxmate.com/pictures/5968e243-1538-49d4-976b-dac3cd8e6687-alert-512.png",
          title: "SOS Activated",
        };
        await sharing.alarm.insert(
          {
            ...o,
            deviceId: _device.id,
            device: _device,
            ruleId: _rule.id,
            userId: _device.userId || 0,
          },
          orgData?.rule?.json.message || "SOS pressed",
          notificationSetting
        );
        _device.signals.set("Mode", "SOS Activated");
      } else if (orgData.svals["0"] === "Emergency.Mode.Disable") {
        await sharing.alarm.dissolve(_device.id, _rule.id, o);
        _device.signals.set("Mode", "SOS Deactivated");
      }
      break;
    }
  }
}
exports.Run = Run;
