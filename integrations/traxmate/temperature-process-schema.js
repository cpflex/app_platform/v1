"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    case "Temperature.Report": {
      const temp = (orgData.vals[0] / 100.0).toFixed(2);
      _device.signals.set("Mode", "Temperature");
      _device.signals.set("Temperature", temp);
      break;
    }
    case "Temperature.TriggerRpt": {
      const idTrig = orgData.vals[0];
      const temp = (orgData.vals[1] / 100.0).toFixed(2);
      _device.signals.set("Mode", "Temp. Trigger");
      _device.signals.set("Temperature", temp);
      _device.signals.set(
        "Status",
        `Trigger (ID = ${idTrig}) threshold exceeded.`
      );
      //TODO Parse into properties.
      break;
    }
    case "Temperature.StatisticsRpt": {
      const min = (orgData.vals[0] / 100.0).toFixed(2);
      const max = (orgData.vals[1] / 100.0).toFixed(2);
      const avg = (orgData.vals[2] / 100.0).toFixed(2);
      const std = (Math.sqrt(orgData.vals[3]) / 100.0).toFixed(2);
      const cnt = orgData.vals[4];
      _device.signals.set("Mode", "Temp. Statistics");
      _device.signals.set(
        "Status",
        `min= ${min} C, avg= ${avg} C, max= ${max} C, stddev = ${std} C, samples = ${cnt}`
      );

      //Save the Stats in as JSON for easy retrieval.
      _device.signals.set("TemperatureStats", {
        min: min,
        mean: avg,
        max: max,
        stddev: std,
        samplecount: cnt,
      });
      break;
    }
    case "Temperature.TriggerDef": {
      _device.signals.set("Mode", "Temp. Trigger Definition");
      _device.signals.set("Status", "Temperature trigger definition uplinked.");
      //TODO Parse into properties.
      break;
    }
    case "Temperature.Config": {
      _device.signals.set("Mode", "Temp. Configuration");
      _device.signals.set(
        "Status",
        "Temperature configuration settings uplinked."
      );
      //TODO Parse into properties.
      break;
    }
  }
}
exports.Run = Run;
