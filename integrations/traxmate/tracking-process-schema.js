"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    case "Track.Mode": {
      _device.signals.set("Mode", "Tracking");
      switch (orgData.svals[0]) {
        case "Track.Mode.Disabled":
          _device.signals.set("Status", "Disabled");
          break;
        case "Track.Mode.Enabled":
          _device.signals.set("Status", "Enabled");
          break;
        case "Track.Mode.Active":
          _device.signals.set("Status", "Active");
          break;
      }
      break;
    }
    case "Track.Config": {
      _device.signals.set("Mode", "Tracking Settings");
      _device.signals.set("Status", "Tracking settings uplinked");
    }
  }
}
exports.Run = Run;
