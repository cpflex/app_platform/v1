"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    case "Loc.Config": {
      _device.signals.set("Mode", "Locate Settings");
      _device.signals.set("Status", "Location module settings uplinked.");
      //TODO Parse into properties.
      break;
    }
  }
}
exports.Run = Run;
