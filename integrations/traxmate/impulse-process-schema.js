"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    case "Impulse.Report": {
      const dur = orgData.vals[1];
      const shock = (Math.sqrt(orgData.vals[2]) * 0.0098).toFixed(3);
      const Xi = (orgData.vals[4] & 0xffff) * dur;
      const Yi = (orgData.vals[6] & 0xffff) * dur;
      const Zi = (orgData.vals[8] & 0xffff) * dur;
      const impulse = (
        Math.sqrt(Xi * Xi + Yi * Yi + Zi * Zi) * 0.000098
      ).toFixed(3);
      const sdur = (dur / 1000.0).toFixed(3);
      _device.signals.set("Mode", "Impulse Event");
      _device.signals.set(
        "Status",
        `Impulse Detected: shock = ${shock} m/sec^2, deltaV = ${impulse} m/sec, duration = ${sdur} sec.`
      );

      //Save Impulse Report:
      _device.signals.set("ImpulseReport", {
        shockMagnitude: shock,
        impulse: {
          magnitude: impulse,
          x: Xi * 0.000098,
          y: Yi * 0.000098,
          z: Zi * 0.000098,
        },
        duration: sdur,
      });
      break;
    }
    case "Impulse.Config": {
      _device.signals.set("Mode", "Impulse Settings");
      _device.signals.set("Status", "Impulse module settings uplinked.");
      //TODO Parse into properties.
      break;
    }
  }
}
exports.Run = Run;
