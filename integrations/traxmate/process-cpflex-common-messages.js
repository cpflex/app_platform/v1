"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

function versionFromUri(uri) {
  if (!uri) return "";
  const idx = uri.lastIndexOf("/");
  if (idx < 0) return uri;
  return uri.substring(idx + 1);
}

async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgdata = o.data.original.data;

  if (!orgdata) {
    return;
  }

  //Save operator and originator data if defined.
  if (orgdata.base_data?.operator) {
    if (!_device.json) _device.json = {};
    _device.json.dataTransferOperator = orgdata.base_data.operator;
    _device.json.dataTransferOriginator = orgdata.base_data.originator;
  }

  //Set received and sent times.
  _device.signals.set("Received Time", new Date().toISOString());
  if (orgdata.sent) {
    _device.signals.set("Sent Time", orgdata.sent);
  }

  //BUG WORK AROUND:  Set the detault positionn time to NULL since Traxmate
  //is setting it to Received time by default, which is not correct.
  //12/4/2023
  _device.signals.set("Position Time", null);

  //Capture prority and category if defined.
  if (orgdata.category) {
    _device.signals.set("Category", orgdata.category);
  }

  if (orgdata.priority) {
    _device.signals.set("Priority", orgdata.priority);
  }

  //Capture App and API versions.
  if (orgdata.appuri) {
    _device.signals.set("App Version", versionFromUri(orgdata.appuri));
  }

  if (orgdata.apiuri) {
    _device.signals.set("API Version", versionFromUri(orgdata.apiuri));
  }

  //Capture if delayed:
  _device.signals.set("Delayed", orgdata.delayed === "true");

  //Extract Common Message Types.
  let msgtype = "";
  if (typeof orgdata.msgtype !== "undefined") {
    msgtype = orgdata.msgtype;
  }
  //Process CP-Flex Standard Messages.
  _device.signals.set("Message ID", msgtype);
  switch (msgtype) {
    // case "LocationReport": {
    //   //Only process location reports that are updated.  We already have
    //   //historical location data.
    //   if (orgdata.updated === true || orgdata.updated === "true") {
    //     _device.signals.set("Mode", "Position");
    //     _device.signals.set("Source", "cps");
    //     _device.signals.set("Speed", 0);
    //     _device.signals.set("Position Time", orgdata.epoch);
    //     const shape = orgdata.location?.shape;
    //     if (shape) {
    //       _device.signals.set("Location", {
    //         lat: shape.lat,
    //         lng: shape.lon,
    //       });
    //       _device.signals.set("Altitude", shape.alt);
    //       _device.signals.set("Accuracy", shape.radius);
    //     }
    //     const miscdata = orgdata.location?.miscdata;

    //     //Update indoor value, clear it if not defined.
    //     let indoor = {};
    //     if (miscdata?.placeid) {
    //       indoor = miscdata.placeid;
    //     }
    //     _device.signals.set("Indoor", indoor);

    //     //Update outdoor value, clear it if not defined
    //     let address = {};
    //     if (orgdata.location?.address) {
    //       address = orgdata.location.address;
    //     }
    //     _device.signals.set("Address", address);
    //   }
    //   break;
    // }
    case "LocationReportV2": {
      const bIsUpdated = orgdata.updated === true || orgdata.updated === "true";
      const bHasPrecise =
        orgdata.positions.length > 0 &&
        (orgdata.positions[0].approximate === false ||
          orgdata.positions[0].approximate === "false");

      //Only process location reports that are updated.  We already have
      //historical location data.
      if (bIsUpdated && bHasPrecise) {
        _device.signals.set("Mode", "Position");
        _device.signals.set("Source", "cps");
        _device.signals.set("Position Time", orgdata.epoch);

        //Set the precise location.
        const pos = orgdata.positions[0];
        if (pos) {
          _device.signals.set("Location", {
            lat: pos.lat,
            lng: pos.lon,
          });
          _device.signals.set("Altitude", pos.alt);
          _device.signals.set("Accuracy", pos.huncert);
        }

        //Update the indoor and address values.
        var indoor = {};
        var address = null;
        if (orgdata.places.length > 0) {
          orgdata.places.forEach((place) => {
            switch (place.type) {
              case "room": {
                indoor.room = place.name;
                break;
              }
              case "floor": {
                if (place.index != null) indoor.floorIndex = place.index;
                if (place.name != null) indoor.floorLabel = place.name;
                break;
              }
              case "building": {
                indoor.building = place.name;
                indoor.buildingId = place.id;
                indoor.buildingModelId = place.calculationModelId;
                break;
              }
            }

            if (address == null && place.address != null) {
              address = place.address;
            }
          });
        }

        _device.signals.set("Indoor", indoor);
        _device.signals.set("Address", address ? address : {});
      }
      break;
    }
    case "StatusMsg": {
      let mode = "Status";
      let statusMsg = "";
      switch (orgdata.code) {
        case 254: {
          //PING
          mode = "Ping";
          statusMsg = "Health check received.";
          _device.json.batteryLevel = +orgdata.battery;
          _device.signals.set(
            "Battery Level" /* BatteryLevel */,
            +orgdata.battery
          );
          break;
        }
        case 0: //Technically unspecified, but there appears to be a bug in the translator.
        case 1: {
          //System Reset
          mode = "Device Reset";
          if (orgdata.message) {
            statusMsg = orgdata.message;
          }
          break;
        }
        case 2: {
          // Duplicate Message Received
          mode = "Duplicate";
          statusMsg = orgdata.message
            ? orgdata.message
            : "Duplicate uplink message detected";
          break;
        }
        default: {
          mode = `Status (code= ${orgdata.code})`;
          if (orgdata.message) {
            statusMsg = orgdata.message;
          }
          break;
        }
      }

      _device.signals.set("Mode", mode);
      _device.signals.set("Status", statusMsg);
      break;
    }
    case "ProximityEntryEvent": {
      _device.signals.set(
        "Mode" /* Mode */,
        "Proximity Enter" /* CloseContact */
      );
      if (orgdata.mac) {
        _device.signals.set(
          "Mode" /* Mode */,
          "Close contact to " + orgdata.mac
        );
      }
      break;
    }
    case "ProximityExitEvent": {
      _device.signals.set("Mode" /* Mode */, "Proximity Exit" /* Leaving */);
      if (orgdata.mac) {
        _device.signals.set("Mode" /* Mode */, "Leaving " + orgdata.mac);
      }
      break;
    }
    case "InvalidMsg": {
      _device.signals.set("Mode", `Error (code=${orgdata.code})`);
      const msg =
        (orgdata.sinfo ? orgdata.sinfo + " " : "") +
        (!orgdata.sinfo && orgdata.id ? "id=" + orgdata.id : "");
      _device.signals.set("Status", msg);
      break;
    }
    case "AppLogMsg": {
      _device.signals.set("Mode", "Log");
      let smsg = orgdata.message ? orgdata.message : "";
      _device.signals.set("Status", smsg);
      break;
    }

    //For flex messages, just define them override with other rules later.
    case "Int32Msg":
    case "KeyValueMsg":
    case "UnencodedMsg": {
      _device.signals.set("Mode", "Flex " + msgtype);
      if (orgdata.nid) {
        _device.signals.set("Message ID", orgdata.nid);
      }
      break;
    }

    //For all other messages, just set the mode to the message type.
    default: {
      // o.data.newData = msgtype;
      _device.signals.set("Mode" /* Mode */, msgtype);
      break;
    }
  }
}
exports.Run = Run;
