"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    case "Power.Charger": {
      _device.signals.set("Mode", "Charger Status");
      switch (orgData.svals[0]) {
        case "Power.Charger.Charging":
          _device.signals.set("Status", "Power Connected");
          break;
        case "Power.Charger.Discharging":
          _device.signals.set("Status", "Power Disconnected");
          break;
        case "Power.Charger.Critical":
          _device.signals.set("Status", "Battery Critical");
          break;
        case "Power.Charger.Charged":
          _device.signals.set("Status", "Battery Charged");
          break;
      }
      break;
    }
    case "Power.Battery": {
      _device.signals.set("Mode", "Battery");
      _device.signals.set("Battery Level", +orgData.vals[0]);
      break;
    }
  }
}
exports.Run = Run;
