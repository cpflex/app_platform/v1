/**
 *  Name:  system.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

forward @SysInit();
forward @SysSleep();
forward @SysWake();

/**
* @brief Event fired whenever network adjusts the devices time.  After event, implementation will reset the device.
*  When time is different, reset is required in order to ensure that driver level timers are appropriately set. 
*  Implement this event to performan any additional cleanup or persistence before the device resets.
* @param  secAdjust  the number of seconds the time was adjusted.
*/
forward @SysRtcAdjusted( secAdjust);

/**
* @brief Executes a system reset on the device.
* @remarks This is an function of last resort.  In ordinary flex programming
* it should never be used as it will not properly cleanup underlying activities
* or state.   This is provided for debugging and test purposes and may be removed later.
*/
native SysReset() = -74;

/**
* @brief Sets the system log filters. These are persisted so they only need to be set once.
* @param archive The archive logging category and priority filter, it is a combination of
*				 MsgCategory and MsgPriority constants defined in Telemdefs.i.  For example capture all 
*				 debug messages archive = MC_debug.  Set to MC_unspecified if not archiving
*				 log message.
*@param terminal The terminal logging category and priority filter, it is a combination of
*				 MsgCategory and MsgPriority constants defined in Telemdefs.i.  For example capture all 
*				 debug messages archive = MC_debug.  Note this only works for devices with
*                terminal output. Set to unspecified if not writing system log to terminal.
*@param uplink The syslog uplink logging category and priority filter, it is a combination of
*				 MsgCategory and MsgPriority constants defined in Telemdefs.i. This is for firmware debugging
*				 only and should be set to zero (0) under normal use.  If used incorrectly, it can 
*				 cause massive uplink traffic. Only categories of none (0), Error, Warning, and Alert are allowed.
*/
native SysSetLogFilters(  MsgCategory: archive, MsgCategory: terminal, MsgCategory: uplink = MsgCategory: 0) = -81;

/**
* @brief Gets the current system log filters. 
* @param archive The archive logging category and priority filter, it is a combination of
*				 MsgCategory and MsgPriority constants defined in Telemdefs.i.  
*@param terminal The terminal logging category and priority filter, it is a combination of
*				 MsgCategory and MsgPriority constants defined in Telemdefs.i.
*@param uplink The uplink logging category and priority filter, it is a combination of
*				 MsgCategory and MsgPriority constants defined in Telemdefs.i.
*/
native SysGetLogFilters(  &archive, &terminal, &uplink) = -82;

/**
* @brief Generates a random number.
* @returns returns the random value.
*/
native SysRand( ) = -85;

/**
* @brief Generates a random number for a range of specified values.
* @param min The minimum allowed value.
* @param max The maximum allowed value.
* @returns returns the random value.
*/
native SysRandr( min, max) = -86;

/**
* @brief Get current system temperature value in units of 0.01 degrees C
* @param value   Current temperature value in 0.01 degrees C.
* @returns Returns result code of operation.
*/
native ResultCode: SysTempGet(  &int:value) = -90;

/**
* @brief Gets system temperature calibration value in units of 0.01 degrees C
* @param bias   Bias value in 0.01 degrees C.
* @returns Returns result code of operation.
*/
native ResultCode: SysTempBiasGet(  &int:bias) = -91;

/**
* @brief Sets system temperature calibration value in units of 0.01 degrees C
* @param bias   Bias value in 0.01 degrees C.
* @returns Returns result code of operation.
*/
native ResultCode: SysTempBiasSet( int: bias) = -92;

