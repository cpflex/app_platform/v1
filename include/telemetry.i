/**
 *  Name:  telemetry.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2021 Codepoint Technologies
 *  All Rights Reserved
 */
#include "telemdefs.i"

/********************************************************************
* Declarations and Definitions
********************************************************************/

/**
* @brief Telemetry state flags indicate the operational state of the telemtry subsystem.
*/
const TelemState: {
	TS_unspecified = 0, //<<< Telemetry network state is unknown.
	TS_disabled = 1,	//<<< Telemetry functions are disabled.
	TS_attachingRestricted = 2, //<<< Device is restricted when attempt to join/attach.
	FTS_attachingTimeout = 3,  //<<< Attaching timed out while attempting to join.
	TS_attaching = 4,   //<<< Device is attempting to attach/join network.
	TS_unavailable = 5, //<<< Telemetry functions enabled, but either device is out of network coverage.
	//TODO add TS_sleeping,  //Add this mode in future
	TS_restricted= 6,   //<<< Device communication is restricted, but eventually available
	TS_busy=7,          //<<< Device is busy.
	TS_available = 8	//<<< Telemetry functions enabled and device is within network coverage last time message was transmitted.
};

const TelemCommClass: {
	TC_CLASS_A = 0,	// Downlinks only checked when uplinking data.
	TC_CLASS_B = 1,	// Scheduled downlinks
	TC_CLASS_C = 2,	// Continuous downlink checks.
	TC_CLASS_D = 3,	// Reserved for future use.
}

/**
* @brief Flags for Posting Sequences.
*/
const TelemPostFlags: {
	TPF_NONE				= 0x0,	// (b0000) No flags.
	TPF_CONFIRM				= 0x1,	// (b0001) Require Confirmation receipt of uplink. 
	TPF_CONFIRM_ARCHIVE	 	= 0x3,	// (b0011) Archive the Sequence if confirmed uplink fails (implied Out of Area)
	TPF_ARCHIVE 			= 0x2,	// (b0010) Archive if network unavailable or out of network (OON).
	TPF_ARCHIVE_NO_UPLINK 	= 0x6,	// (b0110) Archive, but don't uplink (data-logger mode)
};

/********************************************************************
* Telemetry Management
********************************************************************/

/**
* @brief Event hander receives notifications of telemtry system changes.
* @remarks Use this event to trigger other actions when telemetry is available or unavailable.
* @param stateTelem The updated change of telemtry state.
* @param datarate  The datarate in data rate units (e.g. dr0 -dr12).
* @param packetsize The uplink/downlink packetsize in bytes.  -1 is unknown.
*/
forward @TelemStatusEvent( TelemState: stateTelem, datarate, packetsize); 

/**
* @brief Event called to signal processing of archived sequences.
* @remarks Implementing this event overrides the default implementation, which can be called
*  using TelemArchivedSequencesDefault.
* @param ctQueued The current count of sequences in the uplink queue
* @param maxQueue  The maximum count of sequences that can be queued for uplink.
* @param datarate  The current datarate in data rate units (e.g. dr0 -dr12).
* @param packetsize The current uplink/downlink packetsize in bytes.
*/
forward @TelemArchivedSequencesEvent( ctQueued, maxQueue, datarate, packetsize);

/**
* @brief Sets the interval between verification of the link availability and pending downlinks
* @param intvlMinutes  Minimum interval in minutes to perform a link check.
* @param intvlUplinks  Minimum interval in uplink counts to perform a link check.
* @return Returns result code if successful.
*/
native  ResultCode: TelemLinkCheckConfig( intvlMinutes = 15, intvlUplinks= 5) = -83;

/**
* @brief Sets the criteria on failed uplinks to switch telemetry state to Unavailable.
* @remarks Determines when retries are abandoned in favor of reattaching to network.
*  By Default, retries are attempted for first 12 hours, then system will attempt to reattach.
* @param ctFailed  Maximum count of failed confirmed uplinks (-1 disabled)
* @param dtFailed  Time duration (sec) since last successful confirmed uplink (< 1 disabled)
* @return Returns result code if successful.
*/
native  ResultCode: TelemRetryConfig( ctFailed = -1, dtFailed = 43200) = -88;

/**
* @brief Returns the time the last packet of data was sent.
*/
native  TelemLastTransmitPacket() = -53;

/**
* @brief Checks for incoming packets and uplink health status (battery/temp) on network.
*/
native ResultCode: TelemPostHealthStatus() = -54;

/**
* @brief Performs LoRaWAN link check to determine current network connectivity and recieve any pending downlinks.
*/
native ResultCode: TelemLinkCheck() = -103;

/**
* @brief Enables or disables telemetry.  When disabled telemetry messages cannot be sent or received.  Hardware
* will be put in its lowest power mode. When true, messages can be sent or received if in range.
* @param bEnable If true telemetry functions are enabled.  If false, they are disabled.
*/
native TelemEnable( bool: bEnable) = -55;

/**
* @brief Sets the telemetry system communication class.
* @remarks The class determines the frequency of downlink checks and the uplink/downlink packet size.	System defaults to Class A.
*  Only use this function if you are familiar with the LoRaWAN specification and how these modes can affect battery life.
* @param cclass The class of telemetry to use.
* @return Returns result code indicating success or failure.
*/
native ResultCode: TelemSetCommClass( TelemCommClass: cclass) = -93; 

/**
* @brief Returns the current telemetry system status
* @param stateTelem The current state of the telemetry functions.
* @param rss   The received signal strength of last received message.
* @param dtReceived The epoch the last message was received from the network.
* @param ctTxQueue   The count of messages in the transmit queue that are waiting to be sent.
* @param dtTransmit  The epoch the last message was transmitted.
*/
native TelemStatus(  &TelemState:stateTelem, &rss, &dtReceived, &ctTxQueue, &dtTransmit) = -56;

/**
* @brief Returns telemetry link quality metrics.
* @param datarate  The datarate in DR units as specified by the protocol and region.
* @param packetsize The uplink/downlink packetsize in bytes.  -1 is unknown.
*/
native TelemLinkMetrics( &datarate, &packetsize) = -79;

/**
* @brief Clears the current transmit queue
*/
native TelemClearTransmitQueue() = -57;

/**
* @brief Event handler overrides default behavior to handle automated check outs when out of coverage.
* @param stateTelem:  The current state of the Telemetry system.
* @param ctCache:  The number of uplink messages (in local cache, not archive) waiting to be sent.
* @param tLastUplink:  The epoch the last uplink was attempted (success/fail).
* @param tLastConfirmedUplink:  The epoch the last uplink was confirmed (success).
* @param tFirstFailed:  The epoch of the first detected confirmed failure
* @param ctFailed:  The number of uplink failures since last confirmed success.  This increments when retrying. 
* @param ctRestricted: Count of restricted uplinks due to network utilization restrictions since last successful uplink.  If 0 no restrictions.
* @param secNominal:  Outputs the number of seconds until the next uplink.
* @param msecVariance:  Outputs Maximum Variance in msec.
**/
forward @TelemNextScheduled( TelemState: stateTelem, ctCache, tLastUplink, tLastConfirmedUplink, tFirstFailed, ctFailed, ctRestricted, &secNominal, &msecVariance);


/**
* @brief The default scheduler to determine next uplink or join timing.  Can be called from user code to set default values.
* @remarks  This function is called automatically if not specified by application. The following shows 
*   the default algorithm.
*
* Variance is always 5000 msec.
*
* UNAVAILABLE SCHEDULE
*  Interval   |  state  				|                           | Comment
*  -----------------------------------------------------------------------------------------------------------
*     0      |  > unavailable |  ctCache = 0              | No data pending.
*     10     |  > unavailable |  ctCache > 1              | Minimum uplink interval.
*     120    |  > disabled    |                           | Cycles no action 
*     30     |  > disabled    |  < tUnavailable + 60      | Attempt 2 times for first 60 seconds.
*     1800   |  > disabled    |  < tUnavailable + 3600    | Attempt every 30 minutes up to an hour.
*     4200   |  > disabled    |  < tUnavailable + 36000   | Attempt every 1.25  hours up to 10  hours.   
*     28800  |  > disabled    |  < tUnavailable + 86,400  | Attempt every 8  hours up to 24 hours.
*     43200  |  > disabled    |  < tUnavailable + 604,800 | Attempt every 12  hours up to 1 week.   
*     86400  |  > disabled    |  > tUnavailable + 604,800 | Attempt every 24 hours until battery dies.
*
* RETRY SCHEDULE
*  Interval   |          Uplink Offset	   				| Comment
*  -----------------------------------------------------------------------------------------------------------
*     10  	  |  < tLastConfirmedUplink + 60	    | Attempt every FLEX_TELEM_DEFAULT_MINUPLINK_SEC seconds for 1 minute.
*     120     |  < tLastConfirmedUplink + 1800	  | Attempt every 2 minutes for 30 minutes.
*     600     |  < tLastConfirmedUplink + 36000	  | Attempt every 10 minutes for 10 hours.
*     1200    |  < tLastConfirmedUplink + 86400	  | Attempt every 20 minutes for 24 hours.
*     3600    |  > tLastConfirmedUplink + 86400	  | Attempt every hour.
* @param secNominal:  Outputs the number of seconds until the next uplink.
* @param msecVariance:  Outputs Maximum Variance in msec.
**/
native TelemNextScheduledDefault( &secNominal, &msecVariance) = -58;

/**
* @brief Executes the default implementation of the TelemArchivedSequencesEvent.
* @remarks This function can be used when implementing handlers for the event, resulting in the default
*  uplink algorithm.  This allows the event to be overriden with additional conditional checks, then default
*  to the internal archived sequence uplink schedule, which attempts to balance availability and upload speed.
*/
native TelemArchivedSequencesDefault();


/**
* @brief Uplinks request for time check, which responds with time data.
* @returns Returns the ResultCode value.
*/
native ResultCode: TelemRequestTimeCheck() = -87;


/********************************************************************
* Telemetry Uplink (Send) Message API
********************************************************************/

/**
* @brief Begins uplink sequence construction with the specified buffer size.
* @param seq The sequence data object.
* @param sizeBuff Optional maximum buffer size.  Default is 248 bytes.
* @return Returns result code if successful.
*/
native  ResultCode: TelemSendSeq_Begin( & Sequence: seq, sizeBuff = -1 ) = -10;

/**
* @brief Posts a sequence in queue for uplink.  
* @param seq The sequence data object.
* @param flags Post Operation flags.
* @return Returns result code if successful.
*/
native  ResultCode: TelemSendSeq_Post( Sequence: seq, TelemPostFlags: flags = TPF_NONE) = -11;

/**
* @brief Posts a sequence in queue for uplink.  
* @param seq The sequence data object.
* @param bDiscard If true, the sequence is discarded. If false, the sequence is first posted then discarded.
* @param flags Post Operation flags.
* @return Returns result code if successful.
*/
native  ResultCode: TelemSendSeq_End( Sequence: seq, bool: bDiscard, TelemPostFlags: flags = TPF_NONE) = -12;

/**
* @brief Begins a KeyValue message Construction for the specified sequence.
* @param seq The sequence data object.
* @param id The integer key for the entry. Typically, this is an OCM NID definition.
* @param priority Optional priority, default is unspecified.
* @param category Optional category, default is unspecified.
* @return Returns result code.
*/
native  ResultCode: TelemSendKeyValueMsg_Begin( Sequence: seq, id, 
	MsgPriority: priority = MP_unspecified, MsgCategory: category = MC_unspecified) = -13;

/**
* @brief Puts one or more string values into a key value message 
* @param seq The sequence data object.
* @param key The integer key for the entry. Typically, this is an OCM NID definition.
* @param string The string value for the entry.
* @return Returns result code.
*/
native  ResultCode: TelemSendKeyValueMsg_PutString( Sequence: seq, key, const string[]) = -14;

/**
* @brief Puts an integer value into a key value message 
* @param seq The sequence data object.
* @param key The integer key for the entry. Typically, this is an OCM NID definition.
* @param ivalue The integer value for the entry.
* @return Returns result code.
*/
native  ResultCode: TelemSendKeyValueMsg_PutInt( Sequence: seq, key, const ivalue) = -15;

/**
* @brief Constructs an int32 message given array data to send.
* @param seq The sequence data object.
* @param key The integer key for the entry. Typically, this is an OCM NID definition.
* @param iArray The cell array containing the integer data.
* @param len  The length of the values to send .
* @param offset  The starting offset into the array.  Default is 0.
* @return Returns result code.
*/
native  ResultCode: TelemSendKeyValueMsg_PutArray( Flex_Sequence: seq, key, const iArray[], len, offset = 0) = -96

/**
* @brief Puts raw (packed cell array) data into the key value message.  
* @param seq The sequence data object.
* @param raw The raw buffer (packed cell array) of data to send.)
* @param len   The number of bytes in the buffer.
* @param offset The offset into the buffer in bytes.
* @return Returns result code.
*/
native  ResultCode: TelemSendKeyValueMsg_PutRaw( Flex_Sequence: seq, key, const raw[], len, offset = 0) =-16;

/**
* @brief Completes a KeyValue message construction
* @param seq The sequence data object.
* @return Returns result code.
*/
native  ResultCode: TelemSendKeyValueMsg_End( Sequence: seq) = -17;

/**
* @brief Constructs an int32 message to send.
* @param seq The sequence data object.
* @param id The integer identifier for the message. Typically, this is an OCM NID definition.
* @param value The integer value to send.
* @param priority Optional priority, default is unspecified.
* @param category Optional category, default is unspecified.
* @return Returns result code.
*/
native  ResultCode: TelemSendInt32Msg( Sequence: seq, id, const value,
	MsgPriority: priority = MP_unspecified, MsgCategory: category = MC_unspecified) = -18;

/**
* @brief Constructs an int32 message to send.
* @param seq The sequence data object.
* @param id The integer identifier for the message. Typically, this is an OCM NID definition.
* @param iArray The cell array containing the integer data.
* @param len  The length of the values to send .
* @param offset  The starting offset into the array.  Default is 0.
* @param priority Optional priority, default is unspecified.
* @param category Optional category, default is unspecified.
* @return Returns result code.
*/
native  ResultCode: TelemSendInt32MsgArray( Flex_Sequence: seq, id, const iArray[], len, offset = 0,
	MsgPriority: priority = MP_unspecified, MsgCategory: category = MC_unspecified)	 = -95;

/**
* @brief Begins construction of an unencoded message.  These messages are passed untranslated to application in the cloud.
* @param seq The sequence data object.
* @param id The integer identifier for the message. Typically, this is an OCM NID definition.
* @param priority Optional priority, default is unspecified.
* @param category Optional category, default is unspecified.
* @return Returns result code.
*/
native  ResultCode: TelemSendUnencodedMsg_Begin( Sequence: seq, id, 
	MsgPriority: priority = MP_unspecified, MsgCategory: category = MC_unspecified) = -19;

/**
* @brief Puts raw (packed cell array) data into the unencoded message.  
* @param seq The sequence data object.
* @param raw The raw buffer (packed cell array) of data to send.)
* @param len   The number of bytes in the buffer.
* @param offset The offset into the buffer in bytes.
* @return Returns result code.
*/
native  ResultCode: TelemSendUnencodedMsg_PutRaw( Flex_Sequence: seq, const raw[], len, offset = 0) =-20;

/**
* @brief Puts string data into the unencoded message.  
* @param seq The sequence data object.
* @param string An array of one or more strings
* @return Returns result code.
*/
native  ResultCode: TelemSendUnencodedMsg_PutString( Sequence: seq, const string[]) = -21;

/**
* @brief Puts JSON data into the unencoded message.  
* @param seq The sequence data object.
* @param string An array of one or more JSON strings
* @return Returns result code.
*/
native  ResultCode: TelemSendUnencodedMsg_PutJson(Sequence:  seq, const string[]) = -22;

/**
* @brief Completes the unencoded message construction.
* @param seq The sequence data object.
* @return Returns result code.
*/
native  ResultCode: TelemSendUnencodedMsg_End(Sequence:  seq) = -23;

/********************************************************************
* Telemetry Downlink (Receive) Message API
********************************************************************/

/**
* @brief Event handler called when a complete sequence is received.  The sequence may contain multiple messages.
* @param seqIn The input sequence object.
* @param ctMsgs The count of messages contained in the sequence.
* @return Returns result code.
*/
forward @TelemRecvSeq( Sequence: seqIn, ctMsgs);

/**
* @brief Ends received sequence data processing.
* @param seq The received sequence object.
* @return Returns result code.
*/
native  ResultCode: TelemRecv_SeqEnd(Sequence: seq) = -24;

/**
* @brief Gets the next message in the received sequence, returns RC_success if valid.
* @param seq The received sequence object.
* @param msg The message object.
* @param tmsg  The message type.
* @param tcode  The message timecode.
* @param id The integer identifier for the message. Typically, this is an OCM NID definition.
* @param priority Optional priority, default is unspecified.
* @param category Optional category, default is unspecified.
* @return Returns result code.
*/
native  ResultCode: TelemRecv_NextMsg(Sequence: seq, &Message:msg, 
	&MsgType:tmsg, &tcode, &id, &MsgPriority:priority, &MsgCategory:category ) = -25;

/**
* @brief Gets an integer value from the Int32Msg.
* @param msg The message object.
* @param ivalue Reference to the integer variable to store the value.
* @return Returns result code.
*/
native  ResultCode: TelemRecv_ToInt32Msg( Message: msg, &ivalue) = -26;

/**
* @brief Gets an integer value from the Int32Msg.
* @param msg The message object.
* @param maxlen The maximum length of the array.
* @param iarray Reference to the integer array to store the values.
* @param len Reference to the integer variable to store the length of the array.
* @return Returns result code.
*/
native  ResultCode: TelemRecv_ToInt32MsgArray( Message: msg, maxlen, iarray[], &len) = -97;

/**
* @brief Gets Next Key Value message entry in a KeyValueMessage.
* @param msg The message object.
* @param key The key identifier for the entry.  Typically, this is an OCM NID definition.
* @param tdata The data type of the key-value pair.
* @param data Reference to the variable to store entry data.
* @return Returns result code.
*/
native  ResultCode: TelemRecv_NextKeyValueMsgValue( Message: msg, &key, &tdata, &Data:data) =-27;

/**
* @brief Gets Next unencoded message value entry. 
* @param msg The message object.
* @param tdata The data type of the entry.
* @param data Reference to the variable to store entry data.
* @return Returns result code.
*/
native  ResultCode: TelemRecv_NextUnencodedMsgValue( Message: msg, &tdata, &Data:data) = -28;

/**
* @brief Converts received data to a packed string.  Note you must, ensure there is sufficient space.
* @param data  The data object containing a string.
* @param maxlen  The maximum allowed length of the string,  string is truncated if longer.
* @param string  The resulting string.
*/
//native  ResultCode: TelemRecvData_ToString( Data: data, maxlen, string[]) = -29;

/**
* @brief Converts received data to an integer.  
* @param data  The data object containing a string.
* @param ivalue  Reference to the integer value received.
* @param string  The resulting string.
* @return Returns result code.
*/
native  ResultCode: TelemRecvData_ToInt( Data: data, &ivalue) =-30;

/**
* @brief Converts received data to an integer array.
* @param msg The message object.
* @param maxlen The maximum length of the array.
* @param iarray Reference to the integer array to store the values.
* @param len Reference to the integer variable to store the length of the array.
* @return Returns result code.
*/
native  ResultCode: TelemRecvData_ToIntArray( Data: data, maxlen, &iarray, &len) = -98;

/**
* @brief Converts received data to a raw data array.  
* @param data The data object containing the raw data
* @param maxlen The maximum length of the array.
* @param raw  Reference to the raw data buffer (packed cell array).
* @param len Reference to the integer variable to store the length of the array.
* @return Returns result code.
*/
native  ResultCode: TelemRecvData_ToRaw( Data: data, maxlen, &raw, &len) =-31;

/**
* @brief Converts received json data to a packed string.  Note you must, ensure there is sufficient space.
* @param data The data object containing the JSON data.
* @param maxlen  The maximum allowed length of the JSON string,  JSON string is truncated if longer.
* @param json JSON string data.
* @return Returns result code.
*/
//native  ResultCode: TelemRecvData_ToJson( Data: data, maxlen,  json[]) =-32;

