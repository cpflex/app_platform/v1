/**
 *  Name:  datastore.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

#include "telemdefs.i"
#include "log.i"

/***********************************************************************************************
* Non-Volatile Storage API 
***********************************************************************************************/

/**
* @brief Writes a cell of data to non-volatile storage. 
* @param id The non-volatile storage id.
* @param cell The cell to write.
*/
native ResultCode: NonVolatileWrite( id, cell) = -43;

/**
* @brief Reads a cell of data from non-volatile storage.
* @param id The non-volatile storage id.
* @param cell The cell to read into.
*/
native ResultCode: NonVolatileRead( id, &cell) = -44;

/**
* @brief Writes a block of data to non-volatile storage.
* @param id The non-volatile storage id.
* @param cell[] The array to write.
* @param len The number of cells to write.
*/
native ResultCode: NonVolatileWriteArray( id, const cell[], len) = -99;

/**
* @brief Reads a block of data from non-volatile storage.
* @param id The non-volatile storage id.
* @param cell[] The array to read into.
* @param len The number of cells to read.
*/
native ResultCode: NonVolatileReadArray( id, cell[], len) = -100;



/***********************************************************************************************
* Archive API 
***********************************************************************************************/

/**
* @brief Archive record types
*/
const ArchiveRecordType: {
	ART_RAW= 16,			//Record contains unspecified binary data.
	ART_TEXT = 32,			//Record is a text string.
	ART_LOG =  48,			//Record is a log message
	ART_MSG_TRANSMIT  = 80, //Record is an uplink sequence of messages.
	ART_MSG_RECEIVED  = 96,	//Record is a downlink sequence of messages.
	ART_ALL = 240			//Only for PEEK / POP search, use to iterate entire archive.
};

/**
* @brief Archive operation status flags.
*/
const ArchiveOpFlags: {
	AOF_OP_TERMINATED	= 0x01, //Operation is terminated.
	AOF_OP_ERROR		= 0x02, //An error occurred while processing.
	AOF_OP_CANCELLED	= 0x03,	//Operation is cancelled.
	AOF_OP_COMPLETE		= 0x04, //Operation has completed
	AOF_OP_SUSPENDED	= 0x05, //Operation is temporarily suspended.
	AOF_OP_BUSY			= 0x06, //Archive is busy.
	AOF_OP_START		= 0x07, //Archive operation has started
	AOF_OP_STATE_MASK 	=	0x0F,
	AOF_REC_READY   	= 0x40, //Indicates transfer of record data is complete.

};

/** 
* @brief  Archive System Event Flags.
*/
const ArchiveSystemEvent: {
	ASE_ARCHIVE_FULL = 0x1 // Archive is full, default is to erase the archive.
}

/** 
* @brief  Archive System Validation Mode 
*/
const ArchiveValidateMode: {
	AVM_ARCHIVE_VALIDATE = 0 // Validate archive integrity.
	AVM_ARCHIVE_DUMPHDRS = 1 // Validate and dump archive headers to terminal.
	AVM_SYSLOG_VALIDATE  = 2 // Validate system log headers.
	AVM_SYSLOG_DUMP  = 3 	 // Validate and dump system log to terminal.
}


//---------------------------
// Event Handling
//---------------------------
/**
* @brief Called when the Archive System generates an event.
* @remarks If this event is not implemented in the application code, then the behavior will handle the events.
* 
* @param evt Operation Callback Data. Pass to other functions to decode.
*/
forward @ArchiveSystemEvent ( ArchiveSystemEvent: evt);



/**
* @brief Callback archive event handler signature.  This is the signature for callback event notifications.
* @param opData Operation Callback Data. Pass to other functions to decode.
* @param rectype Archive record returned.
* @param flags  Archive Operation flags.
* @param idrec   Archive Record Identifier.
*/
forward @ArchiveOperationHandler ( ArchiveOpData: opdata, rectype, ArchiveOpFlags: flags, idrec);

//---------------------------
// Push Functions
//---------------------------

/**
* @brief Pushes a string message to the Archive.
* @param textdata  String data to add to archive.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchivePushString( const textdata[]) = -36;

/**
* @brief Pushes, raw unspecified data to the archive.
* @param rawdata Raw Data Array (array)
* @param length Length of the array to store.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchivePushArray( const rawdata[],  length) = -37;

/**
* @brief Pushes a sequence of messages to the Archive.
* @param seq  Sequence Object
* @param bIsUplink  If true (default) marks the sequence as an uplink sequence, if false it is a downlink sequence.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchivePushSequence( Sequence: seq, bool: bIsUplink=true) = -76; 


/**
* @brief Pushes custom object data to the archive
* @param rectype  The type of archive record type.  Can be ArchiveRecordType or custom.
* @param dataObject The Data object.
* @param size The size of the dataobject in bytes.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchivePushCustom( rectype, dataObject, size) = -77; 

//---------------------------
// State Functions
//---------------------------

/**
* @brief Clears the archive of the specified record types.
* @param bwait    If true(default), method will block until operation is complete, otherwise
*  It is handled asynchronously.
* @param rectype  The type of archive records to clear.  Can be ArchiveRecordType or custom, Default is all.
* @param dtEarliest Earliest date record should be returned; default is 0, no earliest date.
* @param dtLatest   Latest date records should be returned; default is 0, no lateest date.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchiveClear( bool:bwait=true, rectype = int:ART_ALL, dtEarliest = 0, dtLatest = 0) = -38;

/**
* @brief Erases the entire archive by performing a format operation. This can take a few seconds.  
* @param bwait    If true(default), method will block until operation is complete, otherwise
*  it is handled asynchronously.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchiveErase( bool:bwait=true) = -80;

/**
* @brief Returns the available space remaining in the archive.
* @param freeBytes The number of bytes remaining.  
* @param freeRecords The number of free records remaining.
* @return Returns result code indicating success or failure of method.
*/
native ResultCode: ArchiveFreeSpace( &freeBytes, &freeRecords) = -39;

/**
* @brief Returns the total count of records for the specified type.
* @param count Variable to count of records information.
* @param type  The type of archive records to count.  Default is all.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchiveTotalRecords(&count, ArchiveRecordType: type = ART_ALL) = -40;

/**
* @brief Returns Cancels an archive operation.
* @param opdata Operation data object.
* @return Returns the result code for the call. RC_success if no errors.
*/
native ResultCode: ArchiveCancelOperation( ArchiveOpData: opdata) =-78;

//---------------------------
// Peek/Pop Operation functions.
//---------------------------

/**
* @brief Begins an operation to peek at the records of the specified type.
* @param rectype  The type of archive records to clear.  Can be ArchiveRecordType or custom, Default is all.
* @param dtEarliest Earliest date record should be returned; default is 0, no earliest date.
* @param dtLatest   Latest date records should be returned; default is 0, no lateest date.
* @param offset Starting offset to begin peek operation.
* @param fncNameOpHandler String name of the archive event operation handler function to receive archive event notifications.
* @param bwait If true (default), operation will block until available and complete.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchivePeek( rectype, dtEarliest, dtLatest, offset, fncNameOpHandler[], bool:bwait=true ) = -41;

/**
* @brief Begins an asynchronous operation to pop the records of the specified type.
* @param rectype  The type of archive records to clear.  Can be ArchiveRecordType or custom, Default is all.
* @param dtEarliest Earliest date record should be returned; default is 0, no earliest date.
* @param dtLatest   Latest date records should be returned; default is 0, no lateest date.
* @param offset Starting offset to begin peek operation.
* @param fncNameOpHandler String name of the archive event operation handler function to receive archive event notifications.
* @param bwait If true (default), operation will block until available and complete.
* @return Returns result code indicating operation status.
*/
native ResultCode: ArchivePop( rectype, dtEarliest, dtLatest, offset, fncNameOpHandler[], bool:bwait=true ) = -42;


/**
* @brief Returns archive record header information associated with the specified operation data.
* @param opdata Operation data object.
* @param type  Variable to receive the archive record type.
* @param size  Variable to receive size of the data in the record in bytes.
* @param dtcreated Variable to receive date and time the record was created.
* @param dtmodified Variable to receive date and time the record was created.
* @return Returns the result code for the call. RC_success if no errors.
*/
native ResultCode: ArchiveGetHeaderInfo(ArchiveOpData: opdata, &type, &size, &dtcreated , &dtmodified) = -69;

/**
* @brief Returns raw data (packed cell array) for the specified archive operation data.
* @param opdata Operation data object.
* @param length Actual length of the array.
* @param raw Array to receive the raw data.
* @param maxlen Maximum length of raw packed array can contain in cells.
* @return Returns the result code for the call. RC_success if no errors.
*/
native ResultCode: ArchiveGetArray(ArchiveOpData: opdata, &length, rawdata[], maxlen =sizeof rawdata )= -70;

/**
* @brief Returns packed string data for the specified archive operation data.
* @param opdata Operation data object.
* @param value  Variable to receive the string data.
* @param maxlen Maximum length of the string to copy to value in cells.
* @return Returns the result code for the call. RC_success if no errors.
*/
native ResultCode: ArchiveGetString(ArchiveOpData: opdata, value{}, maxlen =sizeof value) = -71;

/**
* @brief Returns archive log record information.
* @param opdata Operation data object.
* @param msg Variable to receive the packed string message.
* @param category Variable to receive the log message category.
* @param priority Variable to receive the log message priority.
* @param output Variable to receive the LogOutput string flags.
* @param maxlen Maximum length of the string to copy to value in cells.
* @return Returns the result code for the call. RC_success if no errors.
*/
native ResultCode: ArchiveGetLog(ArchiveOpData: opdata, msg{}, 
	&MsgCategory: category, 
	&MsgPriority: priority, 
	&LogOutput: output,
	maxlen = sizeof msg) = -72; 

/**
* @brief Returns archive sequence record information.
* @param opdata Operation data object.
* @param sequence Variable to receive the sequence object pointer.  Ok to use Sequence processing methods while opdata is in scope.
* @return Returns the result code for the call. RC_success if no errors.
*/	
native ResultCode: ArchiveGetSequence(ArchiveOpData: opdata, &sequence) = -73;

/**
* @brief Returns Custom Data object.  This is not directly accessible in Pawn.
* @param opdata Operation data object.
* @param rectype  The type of archive record.  Can be ArchiveRecordType or custom.
* @param dataObject Variable to receive the data object pointer.  
* @return Returns the result code for the call. RC_success if no errors.
*/	
native ResultCode: ArchiveGetCustom(ArchiveOpData: opdata, rectype, &dataObject, &length) = -75;


/***
* @brief Validates the archive and syslog records and returns the result of the operation.
* @param bDump Optional terminal dump of archive record headers.  Only useful for debug builds with terminal interface.
* @return Returns the operation result code.
*/
native ResultCode: ArchiveValidate( ArchiveValidateMode: op = AVM_ARCHIVE_VALIDATE) = -89;
