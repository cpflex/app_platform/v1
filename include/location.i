/**
 *  Name:  location.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

#include "telemdefs.i"

const PositioningMode:
{
	PM_default = 0,
	PM_coarse,
	PM_medium,
	PM_precise,
	PM_all
};

const PositioningTechnologies:
{
	PT_automatic = 0,
	PT_wifi = 1,
	PT_ble = 2,
	PT_wifi_ble = 3,
	PT_gnss = 4,	
	PT_ble_gnss = 5,
	PT_wifi_gnss = 6,

	PT_all = 7
};

native ResultCode: AcquireMeasurements( fncCallbackName[], PositioningMode: mode = PM_default, PositioningTechnologies: technologies = PT_automatic) =-33;

/**
* @brief Returns measurement data for the specified index.
* @param measurements The measurement data containing the data record of interest.
* @param index      The index of the measurement record.
* @param mac        Outputs the mac address as a string (xx:xx:xx:xx:xx:xx). Note preallocate the string by
*					creating an array new str[18];
* @param flags      Outputs the measurement flags: 0=WiFi, 1 = BLE.
* @param rssi       Outputs the received signal strength in dBm.
* @param channel    Outputs the channel the signal was received.
* @param tcLastSeen Outputs the epoch the mac address was last seen.
* @param priority   Outputs the priority of the measurement report.  Default is unspecified. 
*/
native ExtractMeasurement(Measurements: measurements,  index, mac[], &flags, &rssi, &channel, &tcLastSeen)=-59;

/**
* @brief Returns measurement data as an encoded raw cell array
* @param measurements The measurement data containing the data record of interest.
* @param raw		Returned array of raw cell data.
* @param len        Length of the raw cell data array.
*/
native ExtractRawMeasurement(Measurements: measurements,  raw[], &len) =-60;   

/**
* @brief Adds location report to a sequence for uplink send operation.
* @remarks  This replaces the LocationReport method since it allows additional messages to be packed
*   with the measurment data.
* @param seq  The output sequence object to add event data.
* @param measurements The measurement data containing the data record of interest.
* @param epoch The date and time the measurements were made.
* @param bConfirm Indicates if report should require confirmation of receipt by the network. 
*   This should only be used during critical operations.
* @param priority The priority of the measurement report.  Default is unspecified. 
*/
native ResultCode: AttachLocationReport( Sequence: seq, Measurements: measurment,  epoch, MsgPriority: priority = MP_unspecified)=-34;



//Wifi Event callback 
//public OnWiFiAcquire( epoch, ctMeas, Measurements:  measurements);