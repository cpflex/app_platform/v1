
/**
 *  Name:  ui.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

/********************************************************************
* LED API
********************************************************************/

/**
* @brief The led_mode specifies the operating state of the LED.
*/ 
const LedMode: {
	LM_off=0,       ///@ LED is off emitting no light.
	LM_on,  		///@ LED is continuously on.
	LM_blink,		///@ LED is continuosly blinking with specified duration on and off.
	LM_count		///@ LED blinks for a specific count of cycles. 
};

//The following specify the available LEDs for the device.
const LED1=0; ///@brief LED #1 Blue/Green LED
const LED2=1; ///@brief LED #2 Red LED
const LED3=2; ///@brief LED #3 Blue/Green LED
const LED4=3; ///@brief LED #4 Red LED

/**
* @brief Sets the state of the specified LED
* @param id          The LED identifier (e.g. LED1), specifying which LED should be set.
* @param mode        The operating mode to set.  This is any of the led_mode values (e.g. led_mode:continuous).
* @param duration    The duration (or count in the case of led_mode:count) in millseconds to execute, which then returns to led_mode:off.  
* @param msecOn      Number of milliseconds to be on (default is 300 ms);  The whole period is found by adding both on and off intervals.
* @param msecOff     Number of milliseconds to be off (default is 33 ms);  The whole period is found by adding both on and off intervals.
* @param intensity   LED intensity if available.  0 is minimum intensity; 100 is maximum intensity.
* @returns 0 if successfull, non-zero otherwise.
* @remarks When in count mode,  a count of zero produces one on/off pulse, same as a count of one.
*/
native UiSetLed( id,LedMode: mode, duration, msecOn = 250,  msecOff = 250, intensity = 100) = -9;


/********************************************************************
* BUTTON API 
********************************************************************/

const BTN1=0;  ///@brief  Button #1
const BTN2=1;  ///@brief  Button #2
const BTN_BOTH=3; //@brief  Both buttons pressed simultaneously

const ButtonPress:
{
	BP_short = 0, ///@ Short single button press.
	BP_long,	  ///@ Long single button press.	
	BP_double,    ///@ Double short button press (2 short presses). 
	BP_multi,     ///@ Multiple Short button presses greater than 2.  Look at the count of clicks.
	BP_verylong   ///@ Very long button press. 
}

/**
* @brief Button event handler.
* @param idButton  The identifier of the button generating the event.
* @param press    The button press that generated the event.  It will be one of btn_press.
*
**/
forward @UiButtonEvent( idButton, ButtonPress: press, ctClicks);


