/**
*  Name:  power.i
*
*  This module is the proprietary property of Codepoint Technologies
*  Copyright (C) 2019 Codepoint Technologies
*  All Rights Reserved
**/


const PowerEventType: {
	unspecified=0,
	charging,
	discharging,
	chargecomplete
}

/**
* @brief Gets the percent charged indicator for the battery.
* @param pctCharged  Pointer to the inter to contain the percent charged value.
* @param pStatus  Pointer to the power status indication.
* @returns Returns 0 if successful, non-zero if error.
**/
native PowerCurrentStatus( &pctCharged,  & PowerEventType:pStatus ) = -35;

/**
* @brief Power event handler.
* @param eventid  The PowerEventEnum event identifier of the particular event.
* @param pctCharged The current battery charged level in percent (range 0 to 100 percent).
**/
forward @PowerEvent( PowerEventType: pevent,  pctCharged);
