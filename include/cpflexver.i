/**
 *  Name:  cpflexver.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2021-2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

stock const CPFLEX_VERSION{} = "v1.4.5.1";
