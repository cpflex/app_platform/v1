/**
 *  Name:  proximity.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
 #include "telemdefs.i"
 
const ProximityEventType:
{
	PET_entry = 0,
	PET_exit,
	PET_reset  //Proximity Function forcibly reset by firmware (external config change usually Power Charger connection)
};

const ProximityState:
{
	PDS_stop = 0,
	PDS_start,
	PDS_pause,
	PDS_resume
};

const ProximityFilterEventMask:
{
	PFM_ignore = 0,
	PFM_entry  = 1,
	PFM_exit   = 2,
	PFM_all    = 3
};

const ProximityFilterType:
{
	PFT_include = 0,
	PFT_exclude
};

const ProximityModeFlags:
{
	PMF_AUTOPAUSE_ON_LOCATE = 1		
};

/**
* @brief event handler fired whenever motion is detected.
* @param type The type of the motion event.
* @param data Proximity event data.
*/
forward @ProximityEvent( ProximityEventType: type, ProximityEvent: data);


/**
* @brief Enables / Disables Proximity Mode.  
* @remarks When enabled, proximity functions may draw extra power.  Lowest power mode
*  will have proximity detection functions disabled.
* @param bEnable If true, attempts to enable proximity functions, false disables.
* @returns Returns 0 if successful, non-zero if error.
*/
native  ResultCode: ProximityEnable( bool: bEnable) = -61;

/**
* @brief Sets the Proximity detection configuration parameters.
* @param maxRecord  The maximum number of active proximity records allowed at anyone time.  Should be in range of 1 to 128.  More active
*                   records supported, the more RAM consumed  A single record consumes about 48 Bytes, 64 records = 3 KBytes.
* @param intvlScan  The passive scanning mode interval in msec.  This is the time the proximity function is scanning for other devices within one cycle.
* @param intvlAdvertise The advertising mode interval in msec.  This is the time the proximity function advertises within one cycle.
					Set to 0 for no advertising.
* @param intvlSleep The time the proximity function sleeps during a cycle, set to 0 for now sleep.
* @param randModeVariance The randome variance in msec to apply to detection cycle to randomize when sleep and advertise occurs relative to other devices.
* @param ticksRemoveInactive Number of detection cycles (ticks) to wait before removing inactive proximity records.  Default is 5 ticks.
* @param minBleAdIntvl The minimum BLE advertising interval. The minimum channel advertising interval, minimum is 20ms, 
*	which produces the most rapid advertising rate, use for fast detection.
* @param maxBleAdIntvl The maximum BLE advertising interval. The maximum channel adversting interval, This value should be less than the 
*	intervalAdvertise so you get several advertising cycles while in advertising mode.   Min and Max values can be equal if no random dithering is desired.
* @param proxModeFlags Bitflags for setting various Proximity modes.  Default is 0x0000
* @returns Returns 0 if successful, non-zero if error.
*/	
native  ResultCode: ProximitySetParams( 
				maxRecord, intvlScan, intvlAdvertise, intvlSleep, randModeVariance=250,
				ticksRemoveInactive=5, minBleAdIntvl=150, maxBleAdIntvl=300, proxModeFlags= _:PMF_AUTOPAUSE_ON_LOCATE )=-62;
/**
* @brief Creates a ProximityFilter object that can be configured then submitted.
* @remarks  A proximity filter is used to include or exclude one or more MAC addresses using a base MAC address
*   and mask pattern that must match.  This is similar to how an IP address filter works.
*	This function requires the Proximity function be enable in order to succeed.
* @filter    Filter pointer to assign created filter object.
* @ctFilters The count of MacFilters that till be specified.  This must be known in order to allocate memory for the filter object.
* @idFilter  A Filter ID,  this value is provided in the proximity events so it is clear, which filter resulted in generation of a proximity event.
* @priority  When using more than one filter, the priority indicates which is evaluated first.  Higher priority filters (higher value) are evaluated before
*	lower priority filters.  When a filter matches for a particular MAC/RSSI value, no further filters are evaluated.
* @tickEntry Specifies the number of proximity detection cycle tick that must occur before a proximity event is considered active.  Typical values are 0,1, and 2.  
* @rssiEntry The RSSI entry threshold that must be exceeded in order to enter into proximity.   Typical values are -45 to -65 dBM.
* @ticksExit  Specifies the number of proximity detection cycle tick that must occur before a proximity event is considered inactive.  Typical values are 0,1, and 2.  
*	If this value is -1 (default), the ticksEntry are copied.  
* @rssiExit The RSSI exit threshold that must be exceeded in order to enter into proximity.   Typical values are -45 to -65 dBM. If this 
*	value is -1 (default), the rssiExit = rssiEntry - 3.   RSSI on exit should be lower than entry to prevent thrashing. 
* @evtmask  Specifies, which events are generated by the proximity filters,  choices are entry (PFM_entry), exit (PFM_exit), all events (PFM_all default).
* @type		Specifies the filter type, default is an inclusion filter (PFT_include).  Can be PFT_exclude to ignore specified devices matching the MAC_FILTER and RSSI thresholds.
* @fltrWeight Specifies the RSSI smoothing filter weight.  Range is 0 (none) to 8.  Filtering can smooth noisy data, though with increased lag. Default is 3.
*/
native  ResultCode: ProximityCreateFilter( & ProximityFilter:filter, 
			ctFilters, idFilter, priority, ticksEntry, rssiEntry, 
			ticksExit=-1, rssiExit=-1, 
			ProximityFilterEventMask: evtmask=PFM_all, 
			ProximityFilterType: type=PFT_include, fltrWeight=3)=-63;

/**
* @brief Inserts a MAC address filter to the specified ProximityFilter.
* @remarks This function requires the Proximity function be enable in order to succeed.
* @param filter a initialized filter object to insert a mac filter.
* @param index The zero-based index to insert the MAC filter at.  Must be 0 <= index < ctFilter.
* @param mac   The MAC address as a hex string. For example,  "9C8C6E1A588D" represents the mac address 9C:8C:6E:1A:58:8D.
* @param mask  The MAX address mask indicating, which fields must match. For example,
*	"FFFFFFFFFF00" requires that the first 5 bytes of the MAC address must match the specified MAC.
*/
native  ResultCode: ProximityInsertMacFilter( ProximityFilter: filter, index, mac[], mask[])=-64;

/**
* @brief Submits filter created previously and cleans up memory allocation.
* @remarks This function requires the Proximity function be enable in order to succeed.
* @param filter  The filter object to sumbit.  Object is freed once submitted.
*/
native  ResultCode: ProximitySubmitFilter( ProximityFilter: filter)=-65;

/**
* @brief  Clears all existing filters in the proximity function.
* @remarks This function requires the Proximity function be enable in order to succeed.
*/
native  ResultCode: ProximityClearFilters()=-66;

/**
* @brief  Sets the operational state of the proximity function
* @remarks This function requires the Proximity function be enable in order to succeed.
* @param newstate The proximity state to set. See ProximityState for allowed states.
*/
native  ResultCode: ProximitySetState( ProximityState: newstate) =-67;

/**
* @brief Function packs proximity event data into a telemtry sequence.  Add about 15 bytes to sequence buffer to hold proximity event data.
* @param seq  The output sequence object to add event data.
* @param type The type of the motion event.
* @param data Proximity event data.
*/

//TODO Update Name to something Better.
native ResultCode: ProximitySendEvent( Sequence: seq, ProximityEventType: type, ProximityEvent: data)=-68;