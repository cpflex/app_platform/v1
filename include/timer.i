/**
 *  Name:  timer.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */
 #include telemdefs.i

/**
* @brief Specifies infinite cycles.
*/
const TIMER_INFINITE_CYCLES = 0xFFFF;

/**
* @brief Sets an interval timer to receive notifications at the specified intervals.
* @remarks Interval timers function only while processor is awake or in idle mode.   These
*   intervals are disabled when the device is in deep sleep.  Use RTC alarms to wake up during deep
*   sleep.   If timer complete the specified number of ticks they are automatically killed.   
*   Call this function repeatedly to change the timer interval and count; it will reuse underlying structures.
* @param offset Initial offset in milliseconds to initiate the first callback.
* @param interval Interval between callbacks in milliseconds.
* @param count    The number of callbacks to receive. Maximum 65536
* @param pHandle  A pointer to a callback handler.
* @returns Returns RC_success if successful, non-zero if error.
*/
native ResultCode: SetIntervalTimer( offset, interval, count, const fncTimerHandler[]) = -4;

/**
* @brief Timer timeout handler.
* @param tick The count of times the handler has been called.
*/
//forward @TimerHandler( tick);


/**
* @brief Kills the specified timer.
* @param pHandle  A pointer to a callback handler.  This identifies the timer.
* @returns Returns RC_success if successful, non-zero if error.
*/
native ResultCode: KillTimer( fncTimerHandler[]) = -5 ;

/**
* @brief Sets an RTC Alarm to receive notifications at the specified intervals.
* @remarks Alarms function while device is in a deep sleep. If alarms complete the specified number
*  of ticks they are automatically killed.   Call this function repeatedly to change the alarm interval and
*  count; it will reuse underlying structures.
* @param epoch0   Initial RTC epoch in seconds to initiate the first callback.
* @param interval Interval between callbacks in seconds.
* @param count    The number of callbacks to receive.  If TIMER_INFINITE_CYCLES, it never stops.
* @param pHandle  A pointer to a callback handler.
* @returns Returns RC_success if successful, non-zero if error.
*/
native ResultCode: SetRtcAlarm( epoch0, interval, count, const fncTimerHandler[])=-6;

/**
* @brief Kills the specified RTC alarm if defined.
* @param pHandle  A pointer to a callback handler.  This identifies the timer.
* @returns Returns RC_success if successful, non-zero if error.
*/
native ResultCode: KillRtcAlarm( fncTimerHandler[]) =-7;

/**
* @brief RTC Alarm handler.
* @param tick The count of times the handler has been called.
* @param epochNext The time the alarm will trigger next.
*/
//forward @RtcAlarmHandler( tick, epochNext);


/**
* @brief Returns RTC epoch (seconds since 1/1/1970 00:00:00 UTC).
*/
native Now()=-8;

/**
* @brief Returns current OS Tick in milliseconds.
*/
native TimerTick()=-101;

/**
* @brief Sets the RTC time
* @param epoch The RTC epoch to set (seconds since 1/1/1970 00:00:00 UTC).
*/
native RtcSetTime( epoch) = -84;

