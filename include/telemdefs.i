/**
 *  Name:  telemdefs.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */
 
const ResultCode: {
	RC_success = 0,
	RC_failure,
	RC_telem_failure,
	RC_buffer_insufficient,
	RC_buffer_overflow,
	RC_translation_error,
	RC_processing_error,
	RC_not_supported,
	RC_invalid_argument
};

const MsgPriority: {
	MP_unspecified = 0,
	MP_critical = 1,
	MP_high = 2,
	MP_med = 3,
	MP_low = 4,
};

const MsgCategory: {
	MC_unspecified=0,
	MC_error = 16,
	MC_warning = 32,	
	MC_alert = 48,
	MC_info  = 64,
	MC_detail = 80,
	MC_debug = 96
}

const DataType: {
	DT_unspecified=0,
	DT_int = 1,
	DT_string = 2,
	DT_raw  = 3,
	DT_json = 4
}

const MsgType: {
	MT_unspecified=0,
	MT_Reserved1,
	MT_Int32Msg,
	MT_KeyValueMsg,
	MT_UnencodedMsg
};
