/**
 *  Name:  log.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */
#include "telemdefs.i"

const LogOutput: {
	LO_uspecified = 0,
	LO_applog=1,
	LO_syslog=2,
	LO_telemetry=4,		
	LO_all = 15,
	LO_dev = 2,
	LO_default=2
};


/**
* @brief Logs a string message with the specified type, priority, and target output.
* @param msg String message to log.
* @param category  Log message type matches one of the MsgCategory.  
* @param priority Log message priority matches one of the MsgPriority. Default is 'High(2)'.
* @param output  The target output as specified by LogOutput. Default is 'both(2)'
*/
native Log( const msg[], 
	MsgCategory: category, 
	MsgPriority: priority=MP_unspecified, 
	LogOutput: output = LO_default ) = -1;

/**
* @brief Logs a formatted string message with the specified type, priority, and target output.
* @param msg String message to log.
* @param mtype  Log message type matches one of the MsgCategory.  
* @param priority Log message priority matches one of the MsgPriority. Default is 'High(2)'.
* @param output  The target output as specified by LogOutput. Default is 'both(2)'
* @param ...	Formatted arguments.
*/
native LogFmt(const format[], MsgCategory: mtype, 
	MsgPriority: priority, 
	LogOutput: output, {Float,Fixed,_}:...) = -2;

/**
* @brief  Logs a debug category string given optional priority and output.
* @brief This function is typically used for debugging purposes.
* @param msg String message to log.
* @param priority Log message priority matches one of the MsgPriority. Default is unspecified.
* @param output  The target output as specified by LogOutput. Default is LO_dev.
*/
stock TraceDebug( const msg[], 
	MsgPriority: priority=MP_unspecified, 
	LogOutput: output = LO_dev)
{
	Log( msg, MC_debug, priority, output)
}

/**
* @brief  Logs an error category string given optional priority and output.
* @brief This function is typically used for debugging purposes.
* @param msg String message to log.
* @param priority Log message priority matches one of the MsgPriority. Default is unspecified.
* @param output  The target output as specified by LogOutput. Default is LO_dev.
*/
stock TraceError( const msg[], 
	MsgPriority: priority=MP_unspecified, 
	LogOutput: output = LO_dev)
{
	Log( msg, MC_error, priority, output)
}

/**
* @brief  Posts a debug log message given a formatted string with variable arguments.
* @brief This function is typically used for debugging purposes. Output goes to LO_dev
* @param ...	Formatted arguments.
* @param format Message format string.
*/
native TraceDebugFmt( const format[], {Float,Fixed,_}:...) = -3;

