/**
 *  Name:  motion.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */
 
const MotionProfile:
{
	MOP_unknown=0,
	MOP_wakeup,
	MOP_freefall,
	MOP_crash
};

/**
* @brief Constants define the supported motion sampling rates
* @remarks These values are approximate, you must call MotionGetSamplingRate API to get actual sampling rate
*/
const MotionSampling:
{
	MRATE_POWER_DOWN =0,
	MRATE_1HZ,  
	MRATE_10HZ, 
	MRATE_25HZ,
	MRATE_50HZ, 
	MRATE_100HZ
};

/**
* @brief Constants define the supported motions sensing bit resolution.
*/
const MotionResolution:
{
	MRES_HIRES=0, //12 bits
	MRES_NORMAL, //10 bits
	MRES_LOW_POWER //8 bits
};

/**
* @brief Constants define the supported motion sensing range.
*/
const MotionRange:
{
	MRNG_2G=0,  //+- 2G range 
	MRNG_4G,	//+- 4G range
	MRNG_8G,	//+- 8G range
	MRNG_16G	//+- 16G range
}

/**
* @brief Constants define the supported Fifo modes of operation
*/
const MotionFifoMode:
{
	MFM_DISABLED=0,	//Fifo is disabled, acceleration values are always instantaneous.
	MFM_STREAM, //Fifo is in stream mode, the oldest data is overwritten when the buffer is full.
	MFM_FIFO	//Fifo is in FIFO mode, the oldest data is retained when the buffer is full.
};

/**
* @brief event handler fired whenever motion is detected.
* @param profile: The type of the motion event.
* @param magnitude:  Magnitude of the motion average acceleration.
*/
forward @MotionEvent( MotionProfile: profile, magnitude);


/**
* @brief Event handler called whenever Fifo data is available.
* @param x The x access acceleration.
* @param y The y access acceleration.
* @param z The z access acceleration.
* @param bready True if more data is available in current fifo buffer
* @return Return success to continue reading, or any other value to halt reading.
*/
forward ResultCode: @MotionFifoEvent(int: x, int: y, int: z, bool: bready);

/********************************************************************
* Motion Sensor API
********************************************************************/

/**
* @brief Sets the Motion sensor configuration
* @param res  Motion sensing bit resolution.
* @param rate Motion sample rate
* @param mode Motion Fifo operation mode. Default is disabled,  acceleration values are always instantaneous.
* @param range Motion sensing range. Default is 2G.
*/
native MotionSetConfiguration( MotionResolution: res,  MotionSampling: rate, MotionFifoMode: mode= MFM_DISABLED, MotionRange: range = MRNG_2G) = -45;

/**
* @brief Gets current motion sensors actual sampling rate
* @param rate Motion sample rate
* @returns sampling rate in mHz
*/
native int: MotionGetSamplingRate(MotionSamplingRate: rate ) = -102;

/**
* @brief Sets wakeup event monitoring and activates.
* @param duration  Duration in sample periods (1/rate) (set by MotionConfiguration) with range between 0 and 127.
* @param threshold Minimum motion threshold in mg with range between 16 mg and 2000 mg.
* @param bAutoClear  If true, automatically clears the event after registers are read and reported.
*/
native MotionSetWakeupEvent( duration, threshold, bAutoClear) = -46;

/**
* @brief Sets freefall event monitoring and activates.
* @param duration  Duration in sample periods (1/rate) (set by MotionConfiguration) with range between 0 and 127.
* @param threshold Minimum motion threshold in mg with range between 16 mg and 2000 mg.
* @param bAutoClear  If true, automatically clears the event after registers are read and reported.
*/
native MotionSetFreefallEvent( duration, threshold, bAutoClear)  = -47;

/**
* @brief Disables the event generation.  
* @param bPowerDown If true, it will set the configuration to its lowest power state, 
*                   otherwise it leaves it in its current power state.
*/
native MotionDisableEvent( bool: bPowerDown) = -48;

/**
* @brief Clears event allowing next event to occur.
* @remarks  This is only needed if bAutoClear is not set to true when setting the events.
*/
native MotionClearEvent( ) = -49;

/**
* @brief Gets the current acceleration values.
* @param array  3-element array with ax, ay, az in mg.
* @returns Returns true if successful, false otherwise.
*/
native  bool: MotionGetAcceleration( array[])  = -50;

/**
* @brief Begins reading the motion sensor FIFO.
* @remarks  This initiates reading of the fifo firing the "@MotionFifoEvent" event handler.
**/
native ResultCode: MotionFifoBeginRead() = -94;

/**
* @deprecated 	Feb, 2023 - MBM  Use Cora_SysGetTemp instead.
* @brief Gets the motion sensor temperature in celsius
* @remarks  The motion sensor temperature can serve as an approximate temperature
*   reading for the device.   Particularly if the device is in mostly deep sleeping.
* @returns The measured temperature in celsius.
*/
native  MotionGetTemperature( ) = -51;

