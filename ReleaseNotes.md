## v1.4.4.0
Release Date: 231209
1. Improved TemperatureSensor APIs and configurability.

## v1.4.3.3
Release Date: 231209
1. Debugged Tracking Engine implementation.

## v1.4.3.1
Release Date: 231206
1. Added Tracking Engine implementation supports configurable state-machine based location tracking and extension support.
2. Fixed ConfigBattery.i to use proper PAWN syntax.
   
## v1.4.2.0
Release Date: 231020
1. Added TelemLinkCheckApi
2. Renamed TelemCheckForReceivePacket --> TelemPostHealthStatus.  This was only used by the TelemMgr so it was a safe name change to further clarify its purpose.  It posts the standard health message with battery and temperature information.

## v1.4.1.0
Release Date: 231011
1. Added default configuration for Stateful Events.   Previously was not defined.  Use this as a template for creating custom stateful events.
2. Implemented standard downlink processing APIs for battery, MotionTracking, and SimpleTracking that follow the formate developed for other compoenents to simplify application development and integration.

## v1.4.0.3
Release Date: 231007
1. Cleaned up Impulse Sensor and updated code supporting improvements in API.

## v1.4.0.1
Release Date: 230912
1. Fix: build issues with TemperatureSensor and ImpulseSensors.
2. chore: implemented updates to support PawnApi_MotionGetSamplingRate API changes.

## v1.3.3.0
Release Date: 230818
1. Updated Stateful event to support reporting downlinked state changes via configuration flag.
2. Bugfix: fixed stateful event changes to respond to downlink state changes.

## v1.3.2.1
Release Date: 230804
1. Added StatefulEvent module to support easy event notification and management.

## v1.3.1.3
Release Date: 23-04025
### Build V1.3.1.3
  1. Added more standard OCM schemas.
  2. Added integration rules for Traxmate complimenting the OCM schema definitions.
### Build V1.3.1.2
  1. Exposed additional configuration properties on the Location Configuration module. 
### Build V1.3.1.1 (BAD BUILD)
### Build V1.3.1.0
  1. Added min/max tracking offset to SimpleTracking trk_Activate function to provide start up diversity when multiple co-located tags are activated at the same time.  It randomly assigns the offset within the specified range to minimize 
  collisions.


## v1.3.0.4
Release Date: 23-02-26
### Build V1.3.0.4
  1. Bugfix:  Impulse not disabling active state correctly.  
  2. Bugfix:  Impulse losing WOM interrupt notifications.
  3. Bugfix:  Temperature lastTemp not being updated.  Causing endless rate triggers.
  4. References firmware bundle v2.0.12.3a
     * Bugfix  QueueRemove function endless-loop problem (causes crash). 
### Build V1.3.0.3
  1. Updated logging output for impulse and temperature modules to help with in-field testing
### Build V1.3.0.2
  1. Factored out packing algorithms to add rounding.
  2. bugfix: resetting temp last state to zero to avoid needless triggering. Still not sure bug is avoided.
### Build V1.3.0.1
  1. Changed Impulse sensor low power range to a fixed 2G to ensure maximum WOM sensitivity.
### Build V1.3.0.0
  1. Implemented array support for datastore and communications.
  2. Implemented ImpulseSensor and TemperatureSensor Modules.
  3. Added default schemas folder for library modules.
  4. Cleaned up various documentation.
  5. References firmware bundle v2.0.12.2a.
   
## v1.2.13.0
Release Date: 23-01-25
1. Fixed polling Interval bug
2. Changed SetRTCAlarm to use TIMER_INFINITE_CYCLES where appropriate

## v1.2.12.0
Release Date: 23-01-18
1. Added system APIs to support system temperature and bias calibration.

## v1.2.11.1 -  Release for CP-Plex Platform
Release Date: 22-12-22
### Build v1.2.11.1 221222
1. Increased motion-tracker module default sensitivity so works marginally better in cars.
   Now 50mg (was 200mg).
   
### Build v1.2.11.0 221217
1. Updated test apps
2. Updated archive validate API
3. Added freerecords parameter to ArchiveFreeSpace()
4. 
## v1.2.10.0 -  Release for CP-Plex Platform
Release Date: 22-11-09
1. Modified TelemNextScheduled event callback to provide tFirstFailed parameter

## v1.2.9.0 -  Release for CP-Plex Platform
Release Date: 22-10-06
1. Modified TelemNextScheduled event callback to provide additional information about uplink.
   This was a safe API change since no app currently uses this API.   Modificaition allows app
   to provide additional behavior when uplinks are unavailable or retrying.


## v1.2.8.0 -  Release for CP-Plex Platform
Release Date: 22-09-27
1. Added TelemMgr_RadioEnable function.

## v1.2.7.1 -  Release for CP-Plex Platform
Release Date: 21-08-16
1. Added Sysmgr library module for managing archive and basic system functions.
2. Added TelemMgr to provide protocol handling for network configuraion.
3. Updated LocationConfig to support downlink processing.  Added ConfigLoc too.
4. Performed cleanup of other modules to improve reliability.
## v1.2.6.2 -  Release for CP-Plex Platform
Release Date: 21-01-03

Bug Fixes and Updates:
1. **Motion Tracking.p** updates:
   i. Added optional report for zero measurements. Can be enabled by calling trk_ReportZeroMeasurements.
   2. Added additional locate when unit stops tracking.
   3. Re-ordered motion tracking initialization to ensure its properly initialized even if reset during initialization.
   4. Bug fix battery.p incorrectly changing to power save when power is below 50%.
   
## v1.2.5.0 -  Release for CP-Plex Platform
Release Date: 21-11-02

Bug Fixes and Updates:
1. **Motion Tracking.p** updates:
   i. Made all location reports confirmed.

## v1.2.4.0 -  Release for CP-Plex Platform
Release Date: 21-9-07

Bug Fixes and Updates:
1. **SimpleTracking.p** updates:
   i. Added accessor to get the last location report update time:
   trk_TimeLastLocationReport().
   ii. Added boolean switch to uplink log message whenever 0 measurements are scanned.
   trk_ReportZeroMeasurements(). 

## v1.2.3.0 -  Release for CP-Plex Platform
Release Date: 21-6-28

Bug Fixes and Updates:
1) Changed battery discharge reporting algorithm to only allow decreases in battery level.  This should
   reduce the number of redundant battery reports due to normal battery fluctuation.

## v1.2.2.3 -  Release for CP-Plex Platform
Release Date: 21-05-26

Bug Fixes and Updates:
1) Bugfix - Telemetry States Enum now matches platform values, fixes incorrect interpretation of available and unavailable.
2) Changed motion tracking defaults to 1 minute standard and 5 minute emergency mode.  This needs to be refactored to support
   application configurations like SimpleTracking
3) Added PowerSave Disable threshold to work-around bug, where 
   power manager is not reporting charging events, which apparently
   happens ocassionally.  The forces disabling power-save if the battery is above the threshold.
   Configurable using the THRESHOLD_POWERSAVE_DISABLE (ConfigBattery.i).

## v1.2.2.0 -  Release for CP-Plex Platform
Release Date: 21-05-03

**Note Apps compiled with V1.2.2.0 or greater are not backward compatible with previous API and firmware versions.** 
Applications will need to be recompiled to support this API and should only reference V1.0.0.18 or later
versions of the Nali 100 firmware bundle.

Bug Fixes and Updates:
1) Added API to set System RTC.
2) Added API to request cloud-stack time check.
3) Added APIs to generate random numbers
4) Added new TimeAdjust unit test.

## v1.2.1.1 -  Release for CP-Plex Platform
Release Date: 21-04-29

**Note Apps compiled with V1.2.1.0 or greater are not backward compatible with previous API and firmware versions.** 
Applications will need to be recompiled to support this API and should only reference V1.0.0.17 or later
versions of the Nali 100 firmware bundle.

Bug Fixes and Updates:
1) Added support for RtcTimeAdjust
2) Enhanced SysLog configuration API to support telemtry uplink filter.
3) Added 15 second start up delay in SimpleTracking module.  This fixes a startup annoyance, where an acquisition was happening
   prior to full initialization. 
4) Implemented charge complete in Battery Module.
5) Added LEDs activation back to SimpleTracker.

## v1.2.0.0 -  Release for CP-Plex Platform
Release Date: 21-04-14

**Note Apps compiled with V1.2.0.0 or greater  are not backward compatible with previous API and firmware versions.** 
Applications will need to be recompiled to support this API and should only reference V1.0.0.2 or later
versions of firmware.

Bug Fixes and Updates:
1) Added Link Check to Simple and Motion Tracking Modules.
2) Updated unit tests for testing archive and timers.
3) Fixed archive api to work with  v1.0.0.2a or later firmware.
2) various API bug fixes and improvements.
3) Removed telemetry status checks from battery.p.
4) Updated battery.p power save -> critical transition


## v1.1.0.3a -  Release for CP-Plex Platform
Release Date: 21-03-22

**Note Apps compiled with V1.1.0  are not backward compatible with previous API and firmware versions.** 
Applications will need to be recompiled to support this API and should only reference V0.15.0.0 or later
versions of firmware.

Bug Fixes and Updates:
1) Added Compile All for test programs.
2) Updated Datastore APIs with functional archiving support.
3) Fixed Labelling in the LocatoConfig.p
4) Added Library modules and consolidated shared code.
5) Updated Default Telemetry schedule documentation to reflect improvements to firmware.
6) Updated Apis to reflect new firmware,v0.15.0.0, and later.  
7) Generalized battery module with external configuration support.
8) Fixed bug in battery event status reporting.
9) By Popular request Motion Tracking Default interval is 5 minutes
