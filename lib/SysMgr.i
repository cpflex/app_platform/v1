/**
 *  Name:  System.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2022 Codepoint Technologies
 *  All Rights Reserved
 */

 #include "telemetry.i"

forward  SysmgrInit();

/**
* Processes downlink received messages.
* returns the count of messages to uplink.
*/
forward  int: SysmgrDownlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);
