/**
 *  Name:  NvmRecOffsets.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */


 //This modules defines the record offsets for the standard modules.   
 //Records below 0x60 are reserved for user applications.

 const NvmRecordOffsets: {
	 NVRO_TRACKING = 0x60,  //Records for Simple and Motion Tracking implementation
	 NVRO_LOCATION = 0x68,  //Records for Location configuration
	 NVRO_TELEMMGR = 0x70,  //Records for Telemetry Manager.
	 NVRO_BATTERY  = 0x78,  //Records for Battery configuration.
	 NVRO_IMPULSE  = 0x80,  //Records for Impulse configuration.
	 NVRO_TEMPERATURE = 0x88,  //Records for Temperature configuration.
 }