/**
 *  Name:  TemperatureSensor.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2023 Codepoint Technologies
 *  All Rights Reserved
 *
 *  Description:
 *  This module implements impulse detection functions.  It requires dedicated use of the motion sensor
 *  and is not directly compatible with the MotionTracking module.
 */
#include "motion.i" 
#include "telemetry.i"
/*****************************************************
* @brief  Temperature Sensor Callback event flags.
* @remarks Indicates which Temperature Sensor Event has occurred.
*****************************************************/
const  { 
	TSCB_TRIGGERED = 0
};


/**
* Indicates the triggering mode.
*/
const  {
	TTM_ABSGT=0,    //Absolute greater than
	TTM_ABSLT,		//Absolute less than
	TTM_DELTA,		//Relative change either direction
	TTM_DELTAPOS,	//Relative change positive only
	TTM_DELTANEG,	//Relative change negative only
	TTM_RATE,  		//exceeds rate threshold either direction
	TTM_RATEPOS,	//exceeded rate postive only
	TTM_RATENEG		//exceeded rate negative only	
};

/******************************************************************************
* Temperature Sensor Module Callbacks.
******************************************************************************/

/***
* @brief Callback handler implemented by the application to receive
*   notification of temperature sensor state changes and signals.
* @param id The callback event identifier.
* @param idxTrigger The trigger index if defined (-1 is not defined).
* @param temperature  The temperature triggering the event.
*/
forward app_TemperatureSensorEvent(  
	idTscb,
	idxTrigger,
	temperature
);

/******************************************************************************
* Temperature Sensor Module API.
******************************************************************************/

/**
* @brief Initializes impulse sensor module.
*/
forward	temperature_Init();

/**
* @brief Enables the impulse sensor module.
* @brief bEnable If true, enables the module.
*/
forward bool: temperature_Enable(bool: bEnable);

/**
* @brief Toggles Temperature sensor enablement.
* @returns Returns true if the module was enable state was modified.
*/
forward bool: temperature_EnableToggle();

/**
* @brief Indicates true if temperature sensor enabled.
* @returns Returns true if enabled, false otherwise.
*/
forward bool: temperature_IsEnabled();

/**
* @brief Gets the current configuration
* @param config The configuration to get.
*/
forward  temperature_GetConfig( config [ .dtReport, .dtStats, .dtSampling, .flags]); 

/**
* @brief Sets the configuration
* @param config The configuration to set.
*/
forward  temperature_SetConfig( const config[  .dtReport, .dtStats, .dtSampling, .flags]);

/**
* @brief Gets the specified trigger definition.
* @param idx The trigger index.
* @param def The trigger definition to get.
*/
forward  temperature_GetTriggerDef(  idx, def[]);

/**
* @brief Sets the specified trigger definition.
* @param idx The trigger index.
* @param def The trigger definition to set.
*/
forward  temperature_SetTriggerDef(  idx, const def[]);

/**
* @brief Clears the specified trigger definition.
* @param idx The trigger index.
*/
forward  temperature_ClearTriggerDef(idx);

/**
* @brief Clears all trigger definitions.
*/
forward temperature_ClearAllTriggerDefs();

/**
* @brief Procsses downlink messages applicable to impulse module.
* @param seqOut The output sequence to send any uplinks.
* @param tmsg The message type.
* @param id The message id.
* @param msg The message.
* @returns Returns the count of messages posted to seqOut.
*/
forward  int: temperature_Downlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);