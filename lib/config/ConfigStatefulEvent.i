/**
*  Name:  ConfigStatefulEvent.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the stateful event module.
**/

/*************************
* Stateful Events Configuration 
**************************/
#include <StatefulEventDefs.i>
#include <OcmNidDefinitions.i>

const SE_DEF_STATECOUNT = 1;

//FIELDS ARE nidMsg, flags, nidSet, nidClear, priority, category, initialState
stock SE_EVENTDEFS[SE_DEF_STATECOUNT][.nidMsg, .flags, .nidSet, .nidClear, .priority, .category, .initialState] = [
	//Emergency Mode.
	[ 
		_: 0, 
		_: SE_CONFIG_PERSIST_STATE, 
		_: 0, 
		_: 0, 
		_: 0,
		_: 0,
		_: SE_CLEAR
	]
];


