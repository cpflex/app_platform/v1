/**
*  Name:  ConfigTrackingEngine.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2022 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"
#include "telemetry.i"
#include <OcmNidDefinitions.i>
#include "TrackingEngineDefs.i"


/*******************************************************************************
* Tracking Engine General Configuration
*******************************************************************************/
//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.

const _: {
	NM_CMD_CONFIG	= _: NID_TrackConfig,
	NM_INACTIVITY	= _: NID_TrackConfigInactivity,
	NM_NOMINTVL 	= _: NID_TrackConfigNomintvl,
	NM_EMRINTVL		= _: NID_TrackConfigEmrintvl,
	
	NM_CMD_TRACKING	= _: NID_TrackMode,
	NM_ACTIVE		= _: NID_TrackModeActive,
	NM_ACQUIRE		= _: NID_TrackConfigAcquire,
	NM_ENABLE		= _: NID_TrackModeEnabled,
	NM_DISABLE		= _: NID_TrackModeDisabled	
};

const bool:     TRACKING_INIT_ACTIVATE = true;  // If true, tracking is initially active.

// Acquire positioning data indicator
const 			TRACKING_IND_LED_ACTIVATE    = LED1;
const 			TRACKING_IND_LED_DEACTIVATE  = LED2;
const 			TRACKING_IND_COUNT = 4;
const  bool:    TRACKING_IND_ENABLE = true;		//Tracking activation / deactivation indicator is enabled.
const  bool:	ACQUIRE_IND_ENABLE	= true;		//Acquire indicator Enabled
const			ACQUIRE_IND_LED		= LED1;		//LED to use for indication.
const			ACQUIRE_IND_COUNT	= 2;		//Number of cycles to blink.
const			ACQUIRE_IND_ON		= 466;		//Blink on-time in milliseconds.
const			ACQUIRE_IND_OFF		= 133;		//Blink off-time in milliseconds.

// Persisted variable default values.
//These values define the initial value for persisted configurations.
//These are set when the application is first run after installation.
const DEFAULT_ACQUIRE = 50;		//Defines the movement threshold (mg) to begin acquisition.
const DEFAULT_INACTIVITY = 100;     //Defines movement threshold (mg) to test for innactivity
const DEFAULT_INTVL_NORMAL = 1;     //Defines the reporting interval in minutes
const DEFAULT_INTVL_EMERGENCY = 5;   //Defines the reporting interval in minutes

const  TelemPostFlags: DEFAULT_FLAGS_POSTACQUIRE = TPF_ARCHIVE;

/*******************************************************************************
* Tracking Engine State and Properties
*******************************************************************************/

/*******************************
* @brief Enum defines the motion tracking states
********************************/
const TE_STATEDEFS_COUNT = 5;
const  {
	TE_Idle = 0,             //Tracking is active waiting for motion
    TE_VerifyMotion = 1,     // Motion detected verifying it is continuous motion.
    TE_TrackingLocate = 2,   // Active tracking, Locate on enter
    TE_TrackingWaiting = 3,   // Active tracking waiting 30 seconds 
    TE_ExitTracking = 4      //Performs locate on enter, if no more motion, goes to exit tracking, otherwise return to Tracking Locate.
};

/*******************************
* @brief Enum User friendly names of the state.
********************************/
stock const  TE_StateNames[TE_STATEDEFS_COUNT]{5} = [ 
	"IDL", "VFY", "LOC", 
	"LWT", "EXT"
];

/*******************************
* @brief Enum defines Properties define in the Tracking engine.
********************************/
const TE_PROPERTIES_COUNT=1; 
const {
    TEPROP_Prop1 = 0
};

//*******************************
// @brief 1-D array defines properties array (numerically indexed starting at 0).
// @remarks A maximum of 15 properties may be defined.
//*******************************
stock const TE_PROPERTIES_DEFAULT [TE_PROPERTIES_COUNT] = [
    42   //Default is 42
];

/*******************************************************************************
* Tracking Engine State Definitions
*******************************************************************************/

/*******************************
* @brief 2-D array defines the tracking state machine.
********************************/
stock const TE_STATEDEFS [TE_STATEDEFS_COUNT] [
    .flags,  //General State configuration flags.
    .track,  //Tracking configuration flags.
    .motion,  //Motion detection configuration flags.
    .report  //Reporting configuration flags.
] = [

    //STATE TE_Idle - Wait for Motion (1 sample, Motion Enabled)                           
    [  
        0,  // No flags
        0,  //Tracking is disabled
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100),
        TERPT_Enabled | TERPT_RptNoMeasurements | TERPT_POST_SET( TPF_ARCHIVE) | TERPT_PRIORITY_SET(MP_med) 
    ],

    //STATE TE_VerifyMotion - //validate multiple motion ticks in 5 seconds, increase sample rate to verify.
    [
        TESF_SAMPLEINTERVAL_SET(5),  // sample in 5 seconds
        0,  //Tracking is disabled
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling10Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(300),
        0
    ],

    //STATE TE_TrackingLocate - //Execute a locate on Enter and Sampling every 60 seconds.  
    [
        0,  
        TETF_LocateOnEnter, 
        0, // Motion sensing disabled during this state
        TERPT_Enabled | TERPT_RptNoMeasurements | TERPT_POST_SET( TPF_ARCHIVE) | TERPT_PRIORITY_SET(MP_med) 
    ], 

    //STATE TE_TrackingWaiting - //Wait for the specified interval and check if there is motion.
    //Leave reporting enabled so we get updates.
    [
        TESF_SAMPLEINTERVAL_SET(60),  
        TETF_Enabled,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(300),
        TERPT_Enabled | TERPT_RptNoMeasurements | TERPT_POST_SET( TPF_ARCHIVE) | TERPT_PRIORITY_SET(MP_med) 
    ],     

    //STATE TE_ExitTracking  No motion detected, report location and wait one more time.
    [
        TESF_SAMPLEINTERVAL_SET(60),  
        TETF_LocateOnEnter,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100),
        TERPT_Enabled | TERPT_RptNoMeasurements | TERPT_POST_SET( TPF_ARCHIVE) | TERPT_PRIORITY_SET(MP_med) 
    ]
];


/*******************************************************************************
* Tracking Engine Trigger Definitions
*******************************************************************************/

const TE_TRIGGERDEFS_COUNT = 8;
stock const TE_TRIGGERDEFS [TE_TRIGGERDEFS_COUNT][
    .flags,
    .motion, 
    .duration, 
] = [

    //---------------------------------------------------
    // TE_Idle Triggers
    //---------------------------------------------------

    // TRIGGER ON Motion - Any motion detected, transition. 
    [
        TETRIG_IDXSTATE_SET(  TE_Idle ) |  TETRIG_IDXNEXT_SET( TE_VerifyMotion) |  TETRIG_SIGNALS_SET( TESIG_MOTION),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( 0x7FFF),
        0
    ],
 
    //---------------------------------------------------
    // TE_VerifyMotion Triggers
    //---------------------------------------------------

    // TRIGGER ON Motion - Any motion detected within the specified sample interval, transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_VerifyMotion ) |  TETRIG_IDXNEXT_SET( TE_TrackingLocate) |  TETRIG_SIGNALS_SET( TESIG_MOTION),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( 0x7FFF),
        0
    ],
    
    // TRIGGER No Motion - No motion detected wtihin the specified sample interval, transition to Idle.. 
    [
        TETRIG_IDXSTATE_SET( TE_VerifyMotion ) |  TETRIG_IDXNEXT_SET( TE_Idle) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        0,
        TESE_Enabled | TESE_TickSamples | TESE_MINTICKS_SET(1) | TESE_MAXTICKS_SET( 0x7FFF)
    ],    

    //---------------------------------------------------
    // TE_TrackingLocate Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_TrackingLocate ) |  TETRIG_IDXNEXT_SET( TE_TrackingWaiting) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        TEME_Enabled | TEME_MINTICKS_SET(0) | TEME_MAXTICKS_SET( 0x7FFF)
    ],
    
    //---------------------------------------------------
    // TE_TrackingWaiting Triggers
    //---------------------------------------------------

    // TRIGGER On Sample motion detected - Any motion detected within the specified sample interval, transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_TrackingWaiting ) |  TETRIG_IDXNEXT_SET( TE_TrackingLocate) |  TETRIG_SIGNALS_SET( TESIG_MOTION),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( 0x7FFF),
        0
    ],
    
    // TRIGGER On Sample no motion detected - No motion detected within the specified sample interval, transition to tracking exit, 
    [
        TETRIG_IDXSTATE_SET( TE_TrackingWaiting ) |  TETRIG_IDXNEXT_SET( TE_ExitTracking) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        0,
        0
    ],  

    //---------------------------------------------------
    // TE_ExitTracking Triggers
    //---------------------------------------------------

    // TRIGGER On Sample motion detected - Any motion detected within the specified sample interval, transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_ExitTracking ) |  TETRIG_IDXNEXT_SET( TE_TrackingLocate) |  TETRIG_SIGNALS_SET( TESIG_MOTION),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( 0x7FFF),
        0
    ],
    
    // TRIGGER On Sample no motion detected - No motion detected within the specified sample interval, transition to idle, 
    [
        TETRIG_IDXSTATE_SET( TE_ExitTracking ) |  TETRIG_IDXNEXT_SET( TE_Idle) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        0,
        0
    ]
]; 

