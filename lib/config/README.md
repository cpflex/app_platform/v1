# Standard Module Configurations #
This folder contains the default configurations
for the modules.   The library modules look for these
files when compiling and should be added to the include path.

To implement custom configuration, copy the 
configurations folder to your own project and 
reference that folder path in our include paths before the
standard configurations.  Then, update the configurations of interest.  

Additional configuration options will be added so check these 
ocassionally.

