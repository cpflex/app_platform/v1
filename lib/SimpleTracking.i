/**
 *  Name:  SimpleTracking.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 *
 *  Description:
 *  This module implements simple tracking functions supporting interval control and 
 *  toggling activation.
 */




/*****************************************************
* @brief  Callback event flags.
* @remarks Indicates which Motion tracking event has occurred.
*****************************************************/
const  { 
	TCBE_ACQUISITION_COMPLETE = 0,
};


/******************************************************************************
* Simple Tracking Module Callbacks.
******************************************************************************/

/***
* @brief Callback event handler implemented by the application to receive
*   notification of state changes and signals enabled by the state definitions.
* @param id The callback event identifier.
* @param epoch The time of the event.
* @param ctMeas The number of measurements in the event.
*/
forward app_TrkCallbackEvent(  
	idTcbe,
    epoch,
    ctMeas
);

/******************************************************************************
* Simple Tracking Module API.
******************************************************************************/

/**
* @brief Initializes tracking subsystem.
*/
forward	trk_Init();


/**
* @brief Activates the tracking subsystem. 
* @remarks Offset specify random offset range when to start tracking.  This is done to create diversity in start times
*  when lots of devices may be woken at exact same time.  It distributes the load on the network better.  offset = sysrandr(minOffset,maxOffset).
* @param minOffset The minimum offset in seconds to begin tracking from current time. Default is 2 seconds
* @param maxOffset The maximum offset in seconds to begin tracking from current time.   Default is 15 seconds.
*/
forward bool: trk_Activate(minOffset = 2, maxOffset = 15);

/**
* @brief Forces deactivation of the tracking subsystem.
*/
forward bool: trk_Deactivate();

/**
* @brief Toggles activation/deactivation of the tracking subsystem.
*/
forward bool:  trk_ToggleActivate();

/**
* @brief initiates a single Location Acquisition
*/ 
forward bool: trk_Acquire() ; 

/**
* @brief call when entering power saving.  
*/
forward bool: trk_EnterPowerSaving();

/**
* @brief call when exiting power saving.  
*/
forward bool: trk_ExitPowerSaving();  

/**
* @brief sets the acquisition report telemetry post flags, default can be 
* set int ConfigSimpleTracking.i.
* @param flags The post flag value.
*/ 
forward trk_SetPostAcquireFlags(  TelemPostFlags: flags );

/**
* @brief Gets the current acquisition report telemetry post flags.
* @param flags The post flag value.
*/ 
forward TelemPostFlags: trk_GetPostAcquireFlags();

/**
* @brief Enables / Disables Zero Measurement logging
* @param bEnable If true, enables, false disables.
*/
forward trk_ReportZeroMeasurements( bool: bEnable);

/**
* @brief Returns the time a successful location report was uplinked.
* @returns the time the report was made.
*/
forward trk_TimeLastLocationReport();

/**
* @brief Sets interval in minutes
* @param interval The reporting interval in minutes. If zero default is used.
*/
forward trk_SetInterval( interval = 0);

/**
* @brief Gets interval in minutes
* @returns The reporting interval in minutes.
*/
forward trk_GetInterval();

/**
* @brief Adds tracking status report to sequence.
*/
forward ResultCode: trk_ReportStatus( Sequence: seqOut);

/**
* @brief Processes received tracking command message.
*/
forward ResultCode: trk_ProcessCommand(Sequence: seqOut, Message: msg );

/**
* @brief Updates settings given a key value message.
*/
forward trk_UpdateSettings( Message: msg);

/**
* @brief Processes downlink messages applicable to tracking module.
* @param seqOut The output sequence to send any uplinks.
* @param tmsg The message type.
* @param id The message id.
* @param msg The message.
* @returns Returns the count of messages posted to seqOut.
*/
forward  int: trk_Downlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);