/**
 *  Name:  battery.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2021 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

/**
* @brief Initializes Battery and Power management.
*/
forward battery_Init();

/**
* @brief Reports Status of  charging given seq.
*/
forward ResultCode: battery_ReportStatus( Sequence: seq);

/**
* @brief Reports current Charge level
*/
forward ResultCode: battery_ReportChargeLevel(Sequence: seq);

/**
* @brief sends battery status and charge level data in a complete sequence.
*/
forward ResultCode: battery_SendBatteryInfo(bool: bLevelOnly = false, bool: bCritical = false);

/**
* @brief Indicates current charge level by blinking the LED_STATUS_NORMAL and LED_STATUS_CRITICAL defined 
*  in ConfigBattery.i.
*/
forward bool: battery_IndicateChargeLevel();

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
forward app_EnterPowerSaving(pctCharged);

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
forward app_ExitPowerSaving(pctCharged);

/**
* @brief Procsses downlink messages applicable to battery module.
* @param seqOut The output sequence to send any uplinks.
* @param tmsg The message type.
* @param id The message id.
* @param msg The message.
* @returns Returns the count of messages posted to seqOut.
*/
forward  int: battery_Downlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);