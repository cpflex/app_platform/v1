/**
 *  Name:  TrackingEngineDefs.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const IGNORE_VALUE = 0x7FFF;

/******************************************************************************
* State Configuration Definitions
******************************************************************************/

/*****************************************************
* @brief  State Configuration Flags
* @remarks Defines the supported state configuration flags controlling behavior of the
*  state machine when in the specified state.
*****************************************************/
const {
	TESF_CallbackOnEnter  		= 0x00010000 ,   // Executes Callback on entering the state.
	TESF_CallbackOnExit   		= 0x00020000 ,   // Executes Callback on exiting the state.
	TESF_CallbackMotion			= 0x00040000 ,	 // Executes Callback on Motion Signal.
	TESF_CallbackSample			= 0x00080000 ,   // Executes Callback on Sample Signal.
	TESF_CallbackUser1			= 0x00100000 ,   // Executes Callback on User1 Signal.
	TESF_CallbackUser2			= 0x00200000 ,   // Executes Callback on User2 Signal.	
	TESF_CallbackActivate		= 0x00400000 ,   // Executes Callback on Activate Signal.
	TESF_CallbackOnDeactivate 	= 0x00800000 ,   // Executes Callback on Deactivate Signal.
	TESF_CallbackOnAcqComplete  = 0x01000000 ,	 // Executes Callback on Locate Acquisition Complete.
	TESF_Reserved 				= 0xFE000000 ,   // Reserved for future.
    TESF_SHIFT_SampleInterval 	= 0x0,			 // RTC Sampling clock in seconds.  If this is 0, sampling is disabled.
    TESF_MASK_SampleInterval 	= 0x0000FFFF     // If values are between 0xFFF0 and 0xFFFE, the value will be pulled
												 // from the user properties (indexs: 0->E).
};

/******
* @brief Returns the boolean value of the specified flag.
* @param %1   Configuration flags variable.
* @param %2   The configuration flag value to get.
*
**/
#define TESF_FLAG_GET(%1,%2) ( ((%1) & (%2)) != 0)

/******
* @brief Returns the Sample interval in seconds.
* @param %1   Configuration flags variable.
*
**/
#define TESF_SAMPLEINTERVAL_GET(%1) (((%1) & TESF_MASK_SampleInterval) >> TESF_SHIFT_SampleInterval ) 

/******
* @brief Sets the sampling interval in seconds.
* @remarks this is used only during specification of the state configuration flags.   Not intended for use
* in the coded.
* @param %1   The Sample interval value in seconds.
*
**/
#define TESF_SAMPLEINTERVAL_SET(%1) (((%1) << TESF_SHIFT_SampleInterval) & TESF_MASK_SampleInterval) 

//-------------------------------------------------------------------------------------------------------------

/*****************************************************
* @brief  Tracking Configuration   Flags
* @remarks Defines the Tracking configuration, which fits into a 32-bit field.
*****************************************************/
const {
	TETF_Enabled 				= 0x80000000,  // Indicates tracking is enabled whenever any locates are specified.
	TETF_LocateOnEnter    		= 0xC0000000,  // Performs locate on entering state.
	TETF_LocateOnExit     		= 0xA0000000   // Performs locate on exiting state.	
};

/******
* @brief Returns the boolean value of the specified Tracking flag.
* @param %1   Configuration flags variable.
* @param %2   The configuration flag value to get.
*
**/
#define TETF_FLAG_GET(%1,%2) ( ((%1) & (%2)) == (%2))

//-------------------------------------------------------------------------------------------------------------

/*****************************************************
* @brief  Motion Configuration flags
* @remarks Defines the Motion configuration, which fits into a 32-bit field.
*****************************************************/
const{
	TEMF_Enabled    		    = 0x40000000 ,  // If set, Motion detection is enabled for the state.

    TEMF_SHIFT_Profile      	= 28,			//Motion Detector interrupt motion profile.   
    TEMF_MASK_Profile       	= 0x30000000,
    TEMF_ProfileUnknown     	= 0x00000000,	//Unspecified Profile.
	TEMF_ProfileWakeup      	= 0x10000000,	//Wakeup interrupt when motion exceeds threshold and duration values.
	TEMF_ProfileFreefall    	= 0x20000000,	//Freefall interrupt when motion freefall is detected.
	TEMF_ProfileCrash       	= 0x30000000,	//Crash interrupt when motion crash is detected.

    TEMF_SHIFT_Res           	= 26,			//Motion detection resolution.  
    TEMF_MASK_Res            	= 0x0C000000,
    TEMF_ResHires           	= 0x00000000,  	//0 - Hi resolution motion detection. 
    TEMF_ResNormal          	= 0x04000000,  	//1 - Normal resolution motion detection.
    TEMF_ResLowPower	     	= 0x0C000000,  	//2 - Lower power resolution motion detection.

    TEMF_SHIFT_Sampling      	= 23,			// Motion Sampling Rate specifier.
    TEMF_MASK_Sampling       	= 0x03800000,
    TEMF_SamplingPowerDown 		= 0x00000000,  	//0 - No Sampling enabled
    TEMF_Sampling1Hz        	= 0x00800000,  	//1 - 1 Hz sampling.
    TEMF_Sampling10Hz       	= 0x01000000,  	//2 - 10 Hz sampling
    TEMF_Sampling25Hz       	= 0x01800000,  	//3 - 25 Hz sampling.
    TEMF_Sampling50Hz       	= 0x02000000,  	//4 - 50 Hz sampling.
    TEMF_Sampling100Hz      	= 0x02800000,  	//5 - 100 Hz sampling.

    TEMF_SHIFT_Duration      	= 15,			// The duration in seconds over which the threshold is exceeded.
    TEMF_MASK_Duration       	= 0x007F8000,   
    
    TEMF_SHIFT_Threshold  	   	= 0,			// The required threshold to exceed when firing the interrupt.
    TEMF_MASK_Threshold	      	= 0x00007FFF
}

/******
* @brief Returns the boolean value of the specified Motion flag.
* @param %1   Configuration flags variable.
* @param %2   The configuration flag value to get.
*
**/
#define TEMF_FLAG_GET(%1,%2) (((%1)&(%2)) != 0)
#define TEMF_RESOLUTION_GET(%1) (((%1) & TEMF_MASK_Res) >> TEMF_SHIFT_Res ) 
#define TEMF_SAMPLING_GET(%1) (((%1) & TEMF_MASK_Sampling) >> TEMF_SHIFT_Sampling ) 
#define TEMF_PROFILE_GET(%1)  (((%1) & TEMF_MASK_Profile) >> TEMF_SHIFT_Profile ) 

#define TEMF_DURATION_GET(%1)  (((%1) & TEMF_MASK_Duration) >> TEMF_SHIFT_Duration ) 
#define TEMF_DURATION_SET(%1) (((%1) << TEMF_SHIFT_Duration) & TEMF_MASK_Duration)

#define TEMF_THRESHOLD_GET(%1)  (((%1) & TEMF_MASK_Threshold) >> TEMF_SHIFT_Threshold ) 
#define TEMF_THRESHOLD_SET(%1) (((%1) << TEMF_SHIFT_Threshold) & TEMF_MASK_Threshold) 


//-------------------------------------------------------------------------------------------------------------

/*****************************************************
* @brief  Tracking Reporting Configuration  
* @remarks Defines the Tracking data reporting configuration, which fits into a 32-bit field.
*****************************************************/
const {
	TERPT_Enabled    		    = 0x80000000 ,   // If set, Reporting is enabled for the state.
	TERPT_RptNoMeasurements	    = 0x40000000 ,   // If set, report uplinked when no measurements are detected.
	TERPT_MASK_Post				= 0x0000000F,    // Uplink flags mask (Telem Post Flags)
	TERPT_SHIFT_Post			= 0x0,
	TERPT_MASK_Priority			= 0x000000F0,	//  Uplink Priority Flags Mask.
	TERPT_SHIFT_Priority		= 0x4

	//TODO Add uplink NIDS for Entering and exiting state can allocate 8 bits for each. 		 
};
#define TERPT_FLAG_GET(%1,%2) (((%1)&(%2)) != 0)
#define TERPT_POST_GET(%1) (((%1) & TERPT_MASK_Post) >> TERPT_SHIFT_Post) 
#define TERPT_POST_SET(%1) (((_:(%1)) << TERPT_SHIFT_Post) & TERPT_MASK_Post) 
#define TERPT_PRIORITY_GET(%1) (((%1) & TERPT_MASK_Priority) >> TERPT_SHIFT_Priority) 
#define TERPT_PRIORITY_SET(%1) (((_:(%1)) << TERPT_SHIFT_Priority) & TERPT_MASK_Priority) 


/******************************************************************************
* Trigger Definitions
******************************************************************************/

/*****************************************************
* @brief  Tracking Engine Trigger Signal definition.
* @remarks These identify the signals the motion state machine processes.
*****************************************************/
const  {
	TESIG_MOTION 				= 0x1,  //Motion signal asserted
 	TESIG_SAMPLE 				= 0x2,  //Sampling signal  asserted
	TESIG_ACTIVATE 				= 0x4,  //Tracking Engine Activation signal asserted
	TESIG_DEACTIVATE 			= 0x8  //Tracking Engine Deactivation signal asserted.
	TESIG_ENTER_STATE			= 0x10  //Signal raised at completion of entering the state. Useful for immediate state changes
	TESIG_ACQ_COMPLETE			= 0x20, //Signal raised at completion of an acquisition.
	TESIG_USER1 				= 0x40,  //User signal #1 asserted
	TESIG_USER2 				= 0x80,  //User signal #2 asserted
}


/*****************************************************
* @brief  Tracking Engine Trigger Configuration Flags
* @remarks Defines the trigger flags configuration, which fits into a 32-bit field.
*****************************************************/
const {
	TETRIG_SHIFT_IdxState		= 0x0,			//Field defines the state the trigger is associated.
	TETRIG_MASK_IdxState		= 0x0000003F,	

	TETRIG_SHIFT_IdxNext		= 6,			//Field defines the next state to transition if trigger evaluates to true.
	TETRIG_MASK_IdxNext			= 0x00000FC0,		
	
	TETRIG_SHIFT_Signals		= 12,			//Field defines the signals for which this trigger is to be evaluated
	TETRIG_MASK_Signals			= 0x000FF000,	

	TETRIG_User1_Enabled		= 0x40000000,   //User1 value evaluation enabled.   Uses callback to determine if triggered.
	TETRIG_User2_Enabled		= 0x80000000,   //User2 value evaluation enabled.   Uses callback to determine if triggered.
	TETRIG_RESERVED 			= 0x3FF00000
};

#define TETRIG_IDXSTATE_GET(%1)  (((%1) & TETRIG_MASK_IdxState) >> TETRIG_SHIFT_IdxState ) 
#define TETRIG_IDXSTATE_SET(%1)  (((%1) << TETRIG_SHIFT_IdxState) & TETRIG_MASK_IdxState)

#define TETRIG_IDXNEXT_GET(%1)   (((%1) & TETRIG_MASK_IdxNext) >> TETRIG_SHIFT_IdxNext ) 
#define TETRIG_IDXNEXT_SET(%1)   (((%1) << TETRIG_SHIFT_IdxNext) & TETRIG_MASK_IdxNext)

#define TETRIG_SIGNALS_GET(%1)   (((%1) & TETRIG_MASK_Signals) >> TETRIG_SHIFT_Signals ) 
#define TETRIG_SIGNALS_SET(%1)   (((%1) << TETRIG_SHIFT_Signals) & TETRIG_MASK_Signals)
#define TETRIG_USER1_EVAL        (TETRIG_User1_Enabled)
#define TETRIG_USER2_EVAL        (TETRIG_User2_Enabled)
#define TETRIG_FLAG_GET(%1,%2) ( ((%1) & (%2)) != 0)


/*****************************************************
* @brief  Tracking Engine Trigger Motion Evaluator 
* @remarks Defines the motion evaluator configuration, which fits into a 32-bit field.
*****************************************************/
const {
	TEME_Enabled    		    = 0x80000000 ,  // If set, Motion Evaluator is enabled.

	TEME_SHIFT_MinTicks			= 0x0,			//Evaluates true if ticks >= minimum ticks. Ignored if 0x7FFF.
												//If values are between 0x7FF0 and 0x7FFE, the value will be pulled
												//from the user properties (indexes: 0->E).
	TEME_MASK_MinTicks			= 0x00007FFF,	

	TEME_SHIFT_MaxTicks			= 0x16,			//Evaluates true if ticks <= maximum ticks. Ignored if 0x7FFF.
												//If values are between 0x7FF0 and 0x7FFE, the value will be pulled
												//from the user properties (indexes: 0->E).	
	TEME_MASK_MaxTicks			= 0x7FFF0000		
};


#define TEME_FLAG_GET(%1,%2) ( ((%1) & (%2)) != 0)

#define TEME_MINTICKS_GET(%1)  (((%1) & TEME_MASK_MinTicks) >> TEME_SHIFT_MinTicks ) 
#define TEME_MINTICKS_SET(%1) (((%1) << TEME_SHIFT_MinTicks) & TEME_MASK_MinTicks)

#define TEME_MAXTICKS_GET(%1)  (((%1) & TEME_MASK_MaxTicks) >> TEME_SHIFT_MaxTicks ) 
#define TEME_MAXTICKS_SET(%1) (((%1) << TEME_SHIFT_MaxTicks) & TEME_MASK_MaxTicks)

/*****************************************************
* @brief  Tracking Engine Trigger Sample Evaluator 
* @remarks Defines the motion evaluator configuration, which fits into a 32-bit field.
*  The duration evaluator can evaluate seconds since state change or sample ticks,  Set
*  the TESE_TickSamples bit to 1 if using sample ticks.  This will require that the sampling clock is 
*  enabled for the state.   If 0, ticks are seconds since state change.
*****************************************************/
const {
	TESE_Enabled    		    = 0x80000000,  	// If set, Sample Evaluator is enabled.
	TESE_TickSamples			= 0x00008000,	// If set, evaluates sample ticks instead of seconds since state change

	TESE_SHIFT_MinTicks			= 0x0,			//Evaluates true if ticks >= minimum ticks. Ignored if 0x7FFF.
												//If values are between 0x7FF0 and 0x7FFE, the value will be pulled
												//from the user properties (indexes: 0->E).
	TESE_MASK_MinTicks			= 0x00007FFF,	

	TESE_SHIFT_MaxTicks			= 0x16,			//Evaluates true if ticks <= maximum ticks. Ignored if 0x7FFF.
												//If values are between 0x7FF0 and 0x7FFE, the value will be pulled
												//from the user properties (indexes: 0->E).
	TESE_MASK_MaxTicks			= 0x7FFF0000		
};

#define TESE_FLAG_GET(%1,%2) ( ((%1) & (%2)) != 0)

#define TESE_MINTICKS_GET(%1)  (((%1) & TEME_MASK_MinTicks) >> TEME_SHIFT_MinTicks ) 
#define TESE_MINTICKS_SET(%1) (((%1) << TEME_SHIFT_MinTicks) & TEME_MASK_MinTicks)

#define TESE_MAXTICKS_GET(%1)  (((%1) & TEME_MASK_MaxTicks) >> TEME_SHIFT_MaxTicks ) 
#define TESE_MAXTICKS_SET(%1) (((%1) << TEME_SHIFT_MaxTicks) & TEME_MASK_MaxTicks)

/******************************************************************************
* Motion State Definition Template
******************************************************************************/
// *
// * The Motion Tracking module is a reconfigurable state-machine that can be modified to support different functions.
// * This is accomplished by defining an array of states and an array of associated triggers that define the criteria to transition to another state.
// * The Deactivated state is always state 0, which is what the module will initialize.   
// *

//---------------------------------------------------------------------------------------------
// * 1. Create Enum of states and User friendly state names.
//---------------------------------------------------------------------------------------------

// const TE_STATEDEFS_COUNT = 5;
// const  {
// 	TE_Idle = 0,             //Tracking is active waiting for motion
//     TE_VerifyMotion = 1,     // Motion detected verifying it is continuous motion.
//     TE_TrackingLocate = 2,   // Active tracking, Locate on enter
//     TE_TrackingWaiting = 3,  // Active tracking, Waiting for next locate given sample count. If still active at end of period, it will transition back to Locate, 
//                              // otherwise it goes to exit Tracking
//     TE_ExitTracking = 4       //Performs locate on enter, if no more motion, goes to exit tracking, otherwise return to Tracking Locate.
// };

// /*******************************
// * @brief Enum User friendly names of the state.
// ********************************/
// stock const  TE_StateNames[TE_STATEDEFS_COUNT]{5} = [ 
// 	"IDLE", "VRFY", "LOCT", 
// 	"WAIT", "EXIT"
// ];


//---------------------------------------------------------------------------------------------
// * 2. Create Enum of properties and default values.
//---------------------------------------------------------------------------------------------

	/*******************************************************************************
	* Tracking Engine State and Properties
	*******************************************************************************/


	// /*******************************
	// * @brief Enum defines Properties define in the Tracking engine.
	// ********************************/
	// const TE_PROPERTIES_COUNT=1; 
	// stock const {
	//     TEPROP_Prop1 = 0;
	// }

	// //*******************************
	// // @brief 1-D array defines properties array (numerically indexed starting at 0).
	// // @remarks A maximum of 15 properties may be defined.
	// //*******************************
	// const TE_PROPERTIES_DEFAULT [TE_PROPERTIES_COUNT] = [
	// 	32 // The default value of Prop1.
	// ];



//---------------------------------------------------------------------------------------------
// * 3. Define array of state definitions in same order as state identifiers.
//---------------------------------------------------------------------------------------------

	/*******************************************************************************
	* Tracking Engine State Definitions
	*******************************************************************************/

	// //*******************************
	// // @brief 2-D array defines the tracking state machine.
	// //*******************************
	// const TE_STATEDEFS_COUNT = 1;
	// const TE_STATEDEFS  [TE_STATEDEFS_COUNT] [
	//     .flags,  
	//     .track,  
	//     .motion  
	// ] = [ [,,,] ];


//---------------------------------------------------------------------------------------------
// * 4. Define array of trigger definitions associated with the states.
//---------------------------------------------------------------------------------------------

	/*******************************************************************************
	* Tracking Engine Trigger Definitions
	*******************************************************************************/

	// //*******************************
	// // @brief 2-D array defines the triggers associated with each state.
	// //*******************************
	// const TE_TRIGGERDEFS_COUNT = 1;
	// const TE_TRIGGERDEFS [TE_TRIGGERDEFS_COUNT][
	//     .flags,
	//     .motion, 
	//     .duration, 
	// ] = [ [,,,] ];
