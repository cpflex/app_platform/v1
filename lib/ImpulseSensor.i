/**
 *  Name:  ImpulseSensor.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2023 Codepoint Technologies
 *  All Rights Reserved
 *
 *  Description:
 *  This module implements impulse detection functions.  It requires dedicated use of the motion sensor
 *  and is not directly compatible with the MotionTracking module.
 */
#include "motion.i" 
#include "telemetry.i"
/*****************************************************
* @brief  Impulse Sensor Callback event flags.
* @remarks Indicates which Impulse Sensor Event has occurred.
*****************************************************/
const  { 
	ISCB_WOM = 0,
};


/******************************************************************************
* Impulse Sensor Module Callbacks.
******************************************************************************/

/***
* @brief Callback handler implemented by the application to receive
*   notification of state changes and signals.
* @param id The callback event identifier.
* @param stateCur  The array containing the current state data. Array has structure
*  as follows:  [ .idxState, .tickSamples, .tickMotion, .userValue, .tStateChange, .ctMeasured, .tLastLocate].
* @param data The array containing the data. Array has structure
*  as follows:   Rows[3] are Physical Axes (X,Y,Z) and
*  Columns are[4]:  [ Max Accel (mg), Min Accel (mg), Delta V (mgs), |Delta V (mgs)|.
*/
forward app_ImpulseSensorEvent(  
	id,
	const stateCur[ .flags, .tActive, .ctRead, .dt, .ctSamples, .maxAccel ],
    const data[3][4]
);

/******************************************************************************
* Impulse Sensor Module API.
******************************************************************************/

/**
* @brief Initializes impulse sensor module.
*/
forward	impulse_Init();

/**
* @brief Enables the impulse sensor module.
* @brief bEnable If true, enables the module.
*/
forward bool: impulse_Enable(bool: bEnable);

/**
* @brief Gets the current configuration
* @param config The configuration to get.
*/
forward  impulse_GetConfig( config [ 
    MotionSampling: .rateLowPwr, MotionSampling: .rateActive,
    MotionRange: .range, int: .thrshWom,  
    int: .durWom, int: .thrshAccel, int: .thrshEnd,
    _: .flags  
]);

/**
* @brief Sets the configuration
* @param config The configuration to set.
*/
forward  impulse_SetConfig( const config[ 
    MotionSampling: .rateLowPwr, MotionSampling: .rateActive,
    MotionRange: .range, int: .thrshWom,  
    int: .durWom, int: .thrshAccel, int: .thrshEnd,
    _: .flags  
])

/**
* @brief Procsses downlink messages applicable to impulse module.
* @param seqOut The output sequence to send any uplinks.
* @param tmsg The message type.
* @param id The message id.
* @param msg The message.
* @returns Returns the count of messages posted to seqOut.
*/
forward  int: impulse_Downlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);
