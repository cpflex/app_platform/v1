/**
 *  Name:  MotionTracking.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 *
 *  Description:
 *  This module implements advanced tracking functions supporting accelerometer motion activation/deactivation.
 */

/**
* @brief Initializes tracking subsystem.
*/
forward	trk_Init();

/**
* @brief Forces activation of the tracking subsystem.
*/
forward bool: trk_Activate();

/**
* @brief Forces deactivation of the tracking subsystem.
*/
forward bool: trk_Deactivate();

/**
* @brief Toggles activation/deactivation of the tracking subsystem.
*/
forward bool:  trk_ToggleActivate();

/**
* @brief initiates a single Location Acquisition if in active tracking state.
*/ 
forward bool: trk_Acquire() ; 

/**
* @brief call when entering power saving.  
*/
forward bool: trk_EnterPowerSaving();

/**
* @brief call when exiting power saving.  
*/
forward bool: trk_ExitPowerSaving();  

/**
* @brief Sets either the emergency or normal interval in minutes
* @param bEmergency If true, emergency interval updated.
* @param interval The reporting interval in minutes. If zero default is used.
*/
forward trk_SetInterval( bool: bEmergency,  interval = 0);

/**
* @brief Adds tracking status report to sequence.
*/
forward ResultCode: trk_ReportStatus(Sequence: seqOut);

/**
* @brief Processes received tracking command message.
*/
forward ResultCode: trk_ProcessCommand(Sequence: seqOut, Message: msg );

/**
* @brief Updates settings given a key value message.
*/
forward trk_UpdateSettings( Message: msg);

/**
* @brief Processes downlink messages applicable to tracking module.
* @param seqOut The output sequence to send any uplinks.
* @param tmsg The message type.
* @param id The message id.
* @param msg The message.
* @returns Returns the count of messages posted to seqOut.
*/
forward  int: trk_Downlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);

/**
* @brief Adds tracking settings report to the sequence.
*/
forward ResultCode: trk_ReportSettings( Sequence: seq);

