/**
 *  Name:  StatefulEventDefs.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2023 Codepoint Technologies
 *  All Rights Reserved
 */

/*****************************************************
* @brief  Stateful event state values.
*****************************************************/
const  StatefulEventState: { 
    SE_ERROR = -2,
    SE_UNKNOWN = -1,
	SE_CLEAR = 0,
    SE_SET = 1,
};

const SE_CONFIG_PERSIST_STATE = 1; //State will persist between reboots.
const SE_CONFIG_TRANSIENT = 2; // State changes are transient previous state is always unknown.
const SE_CONFIG_RPT_DOWNLINK = 4; //Reports downlink state changes.
