/**
 *  Name:  MotionTracking.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2021 Codepoint Technologies
 *  All Rights Reserved
 *
 *  Description:
 *  This module implements advanced tracking functions supporting accelerometer motion activation/deactivation.
 */


/*****************************************************
* @brief  Callback event flags.
* @remarks Indicates which Motion tracking event has occurred.
*****************************************************/
const  { 
	TCBE_StateOnEnter = 0,   		//Callback fired whenever entering a state
	TCBE_StateOnExit,				//Callback fired whenever exiting a state.
	TCBE_SignalMotion,				//Callback fired whenever motion is detected.
	TCBE_SignalSample,				//Callback fired whenever sampling clock timesout.
	TCBE_CallbackUser1				//Callback fired whenever user1 is signaled.
	TCBE_CallbackUser2				//Callback fired whenever user1 is signaled.
	TCBE_SignalActivate,			//Callback fired whenever Tracking is activated.
	TCBE_SignalDeactivate,			//Callback fired whenever Tracking is deactivated.
	TCBE_SignalAcqComplete,			//Callback fired whenever measurement acquisition is completed.
	TCBE_EvaluateUserValue1,	    //Callback fired whenever needing to evaluate user value 1.
	TCBE_EvaluateUserValue2,        //Callback fired whenever needing to evaluate user value 2.
};


/******************************************************************************
* Tracking Engine Module Callbacks.
******************************************************************************/

/***
* @brief Callback event handler implemented by the application to receive
*   notification of state changes and signals enabled by the state definitions.
* @param id The callback event identifier.
* @param stateCur  The array containing the current state data. Array has structure
*  as follows:  [ .idxState, .tickSamples, .tickMotion, .userValue1, .userValue2,  .tStateChange, .ctMeasured, .tLastLocate].
* @return Returns true or false on evaluation callbacks otherwise returns true.
*/
forward bool: trk_CallbackEvent(  
	id,
	const stateCur[ .idxState, .tickSamples, .tickMotion, .userValue1, .userValue2, .tStateChange, .ctMeasured, .tLastLocate],
	const properties[]
);

/******************************************************************************
* Tracking Engine Module API.
******************************************************************************/

/**
* @brief Initializes tracking subsystem.
*/
forward	trk_Init();

/**
* @brief Forces activation of the tracking subsystem, sets state to 0
*/
forward bool: trk_Activate();

/**
* @brief Forces deactivation of the tracking subsystem.
*/
forward bool: trk_Deactivate();

/**
* @brief Toggles activation/deactivation of the tracking subsystem.
*/
forward bool:  trk_ToggleActivate();


/**
* @brief Sets the Tracking Engine to the specified state
* @returns Returns true if successful, false if engine is not in an activated state.
*/
forward bool: trk_SetState( newState) ;

/**
* @brief Sets the specified property value
* @param idxProp  The property index.
* @param value    The numerical value to set.
*/
forward bool: trk_SetProperty( idxProp, value);

/**
* @brief Gets the specified property value
* @param idxProp  The property index.
* @param value    The numerical value pointer to get the value.
* @returns Returns true if successful, false otherwise.
*/
forward  bool: trk_GetProperty( idxProp, &value);



/**
* @brief Updates user defined value #1 triggering state machine update.  Unchanged values
* are not processed. 
*/
forward bool: trk_SignalUser1( value );

/**
* @brief Updates user defined value #2 triggering state machine update.  Unchanged values
* are not processed. 
*/
forward bool: trk_SignalUser2( value );

/**
* @brief initiates a single Location Acquisition if in active tracking state.
*/ 
forward bool: trk_Acquire() ; 

/**
* @brief call when entering power saving.  
*/
forward bool: trk_EnterPowerSaving();

/**
* @brief call when exiting power saving.  
*/
forward bool: trk_ExitPowerSaving();  

/**
* @brief Sets either the emergency or normal interval in minutes
* @param bEmergency If true, emergency interval updated.
* @param interval The reporting interval in minutes. If zero default is used.
*/
forward trk_SetInterval( bool: bEmergency,  interval = 0);

/**
* @brief Adds tracking status report to sequence.
*/
forward ResultCode: trk_ReportStatus(Sequence: seqOut);

/**
* @brief Processes received tracking command message.
*/
forward ResultCode: trk_ProcessCommand(Sequence: seqOut, Message: msg );

/**
* @brief Updates settings given a key value message.
*/
forward trk_UpdateSettings( Message: msg);

/**
* @brief Adds tracking settings report to the sequence.
*/
forward ResultCode: trk_ReportSettings( Sequence: seq);