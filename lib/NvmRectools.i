/**
 *  Name:  NvmRecTools.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2021 Codepoint Technologies
 *  All Rights Reserved
 */

#include "telemdefs.i"

forward ResultCode: nvmrec_init(nvmrec: id, value);	

/**
* @brief  Initialize NVM array record.  This function is called once per NVM record to initialize the record.
* @param  id - NVM record ID
* @param  array[] - array to write
* @param  size - size of array	
*/
forward ResultCode: nvmrec_initArray(nvmrec: id, const array[], size);

/**
* @brief  Check if NVM record exists.  This function is called to check if NVM record exists.
* @param  id - NVM record ID
*/
forward bool: nvmrec_exists(nvmrec: id);

/**
* @brief  Update NVM record.  This function is called to update the NVM record.
* @param  id - NVM record ID
* @param  value - new value of NVM
 */
forward ResultCode: nvmrec_update(nvmrec: id, value);

/**
* @brief  Read NVM record.  This function is called to read the NVM record.
* @param  id - NVM record ID
*/
forward nvmrec_get(nvmrec: id);


/**
* @brief  Read an array from NVM
* @param  id - NVM record ID
* @param  array[] - array to read into
* @param  size - size of array	
 */
forward ResultCode: nvmrec_getArray( nvmrec: id, array[], size);

/**
* @brief  Write an array to NVM
* @param  id - NVM record ID
* @param  array[] - array to write
* @param  size - size of array	
*/
forward ResultCode: nvmrec_setArray( nvmrec: id, const array[], size);