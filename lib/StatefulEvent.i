/**
 *  Name:  StatefulEvent.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2023 Codepoint Technologies
 *  All Rights Reserved
 */

#include "telemetry.i"
#include "StatefulEventDefs.i"



/******************************************************************************
* StatefulEvent Module Callbacks.
******************************************************************************/

/***
* @brief Callback event handler implemented by the application to receive
*   notification of event state changes.
* @param idEvent The stateful event identifier.
* @param newState  The new state identifier.
*/
forward app_StatefulEventCallback(  idEvent, StatefulEventState: newState);


/******************************************************************************
* Stateful Event Module API
******************************************************************************/

/***
* @brief Initializes the stateful event module.
*/
forward  StatefulEventInit();

/***
* @brief Sets the stateful event.
* @param idEvent The stateful event identifier.
* @param newState  The new state identifier.
* @param bNotify If true (default), uplink notification is posted.
*/
forward StatefulEventState: StatefulEventSet( idEvent, StatefulEventState: newState, bool: bNotify = true);

/***
* @brief Gets the stateful event.
* @param idEvent The stateful event identifier.
* @returns  The current state identifier.
*/
forward StatefulEventState: StatefulEventGet( idEvent);

/**
* Processes downlink received messages.
* @param seqOut Output sequence to store uplink information.
* @param tmg Downlink sequence message type.
* @param id  The message identifier.
* @param msg The received downlink message.
* @returns the count of messages to uplink.
*/
forward  int: StatefulEventDownlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);
