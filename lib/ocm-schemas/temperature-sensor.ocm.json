{
  "title": "Temperature Sensor OCM Protocol Definition",
  "copyright": "Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.",
  "uuid": "1c789d74-76d5-4d1f-9db2-012bd1968428",
  "version": {
    "major": 1,
    "minor": 0,
    "maintenance": 0,
    "revision": 0,
    "dateModified": "2023-02-14T12:00:00.000Z"
  },
  "authors": "M.B. Mathews",
  "namespaces": [
    {
      "nid": "Temperature",
      "elements": [
        {
          "nid": "Report",
          "etype": "Int32Msg",
          "description": "Uplink message reporting the current temperature  in centi-degrees Celius."
        },
        {
          "nid": "TriggerRpt",
          "etype": "Int32Msg",
          "description": "Uplink message reporting the specified temperature trigger was exceeded. \nColumns are defined as follows: [ id, temperature]\nwhere:\n* id          -- trigger ID valid range 0 to 3 (max 4 triggers);\n* temperature -- temperature value in centi-degrees (1/100th C).\n"
        },
        {
          "nid": "StatisticsRpt",
          "etype": "Int32Msg",
          "description": "Uplink message reporting temperature statistics.\nColumns are defined as follows: [ id, mintemp, maxtemp, avgTemp, variance, count]\nwhere:\n* mintemp -- minimum temperature value in centi-degrees (1/100th C or cC).          \n* maxtemp -- maximum temperature value in centi-degrees (1/100th C or cC).          \n* avgtemp -- average temperature value in centi-degrees (1/100th C or cC).          \n* variance -- The calculated variance (take square root to get std. deviation).\n* count -- The count of samples in the last period.\n"
        },
        {
          "nid": "TriggerDef",
          "etype": "Int32Msg",
          "description": "TrigDef message either sets or gets (id only) the trigger definition specified by the ID field.\n[ id, threshold, hysteresis, mode]. Set threshold, hysteresis, and mode to -1 if trigger is disabled.\nwhere:  \n* id         -- trigger ID valid range 0 to 3 (max 4 triggers). If -1, not defined (default)\n* threshold  -- trigger threshold value in centi-degrees (1/100th C or cC).\n* hysteresis -- trigger hysteresis value in cC\n* mode       -- trigger evaluation mode: 0=ABSGT, 1=ABSLT, 2=DELTA, 3=DELTAPOS, 4=DELTANEG, 5=RATE, 6=RATEPOS, 7=RATENEG.\n"
        },
        {
          "nid": "ClearTriggers",
          "etype": "Int32Msg",
          "description": "Clears all the triggers.\n"
        },
        {
          "nid": "Config",
          "etype": "KeyValueMsg",
          "description": "Temperature sensor base configuration settings.\n",
          "entries": [
            {
              "nid": "dtReport",
              "vtype": "int32",
              "description": "Reporting interval in minutes, -1 is disabled (default).",
              "defaultValue": -1
            },
            {
              "nid": "dtStats",
              "vtype": "int32",
              "description": "Stats reporting interval in minutes, -1 is disabled(default)"
            },
            {
              "nid": "dtSampling",
              "vtype": "int32",
              "description": "Sampling interval in seconds, -1 is disabled (default).",
              "defaultValue": -1
            },
            {
              "nid": "flags",
              "vtype": "int32",
              "description": "Specifies one or more bit flags to control sensor module behavior\nConfiguration flags  \n0x0001:  Confirm triggered reports: confirmed (1), unconfirmed (0)\n0x0002:  Confirm statistics reports: confirmed (1), unconfirmed (0)\n"
            }
          ]
        }
      ]
    }
  ]
}
