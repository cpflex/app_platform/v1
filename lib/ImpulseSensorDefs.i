/**
 *  Name:  ImpulseSensorDefs.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 *
 *  Description:
 *  This module implements impulse sensor functionality using the 3-axis accelerometer.
 */

#define ISF_ENABLED 1 //Indicates impulse sensor is enabled.
#define ISF_DETECTOR_DISABLED 2 //Disable the impulse detection filter.  Effectively
                               //turns off impulse detection while allowing basic 
                               //motion detector to work and continue firing WOM events.
