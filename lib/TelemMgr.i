/**
 *  Name:  TelemMgr.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

/**
* @brief Initializes Telemetry management functions.
*/
forward TelemMgr_Init();

/**
* @brief Sets the polling interval, -1 is disabled, units are in minutes.
*/
forward TelemMgr_SetPollingInterval( intvl);

/**
* @brief Reports configuration settings of telemetry management.
*/
forward TelemMgr_ReportSettings( seqOut);

/**
* @brief Processes Telemetry management settings key value message.
*/
forward TelemMgr_UpdateSettings( msg);

/**
* @brief provides a visual indication of network status.
*/
forward TelemMgr_IndicateNetworkStatus();

/**
* @brief Processes downlink received messages.
* @returns Returns the count of messages to uplink.
*/
forward int: TelemMgr_Downlink( Sequence: seqOut, MsgType: tmsg, id, Message: msg);


/**
* @brief Checks current Radio state and enable/disables as needed
* @param delay in seconds before disabling radio (applicable on disable only)
*/
forward TelemMgr_EnableRadio(bool: bEnable, delay = 90);