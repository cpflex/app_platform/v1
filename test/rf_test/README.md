# Nali - N100 RF Transmit Receive Test

The Flex application provides functions for testing LoRaWAN transmit and receive functions. It does not
have control over frequencies.

## Instructions ##
1. Follow the instructions in the user's guide to install the rf_test.bin file.
2. The source code is in [rf_test.p](rf_test.p), it can be modified to suit your additional needs.

## RF Test User Interface Description ## 

When automatically sending messages, the tag does not request message confirmation.  If sent 
manually, the tag will request confirmation of receipt.s
 
### Normal Operating Mode 
Normal operating mode is entered when the device boots.

**Operating Mode Button Descriptions**
 
| Button 1 | Button 2 | Description |
|---|---|---|
| 1 quick press|  |  Sends location message (confirmation requested) |
| 1 long press (> 1.5 sec.)|	| Activates / Deactivates Rapid transmission of messages every 5 seconds (Max 10,000 cycles). |
|  | 1 quick press   | Indicates battery level (see Battery Indicator below) |
|  | 2 quick presses | Indicates network coverage status (see Coverage Indicator below) |
| >15 sec. press | | Resets the device. |

**Operating Mode Button Descriptions** 

| LED #1 | LED #2 | Description |
|---|---|---|
| Blue blink three times | Blue blink three times | Device restarted |
| 2 blinks |              |Device is sending a message.
| Red blink three times | | Input invalid. |
| Blue blink twice | | Auto send activated.  |
| Red blink twice | | Auto send deactivated. |
| | Purple slow blink every 10 sec. | Battery is charging. |
| | Blue slow blink every 10 sec.  | Battery is charged. |
| | Four(4) red blinks every two minutes | Battery is nearly dead, place tag on charger |


 ### Battery Indicator 

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2  | % Charge    | Description|
|---------|-------------|------------|
|  1 red  |   < 5%      | Battery level is critical,  charge immediately. |
|  1 blue |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 blue |  30% - 50%  | Battery is about 40% charged. |
|  3 blue |  50% - 70%  | Battery is about 60% charged. |
|  4 blue |  70% - 90%  | Battery  is about 80% charged. |
|  5 blue |  > 90%      | Battery is fully charged. |

### Network Coverage Indicator 

Quickly pressing Button #2 twice will indicate the status of LoRaWAN network coverage.  

LED #2 will blink as follows to indicate coverage and network status information

| LED #2  | Signal Strength (dBm) | Description                          |
|---------|-----------------------|--------------------------------------|
|  4 blue |  -64  to -30    | Very strong signal strength                |
|  3 blue |  -89  to  -65   | Good signal strength                       |
|  2 blue |  -109  to -90   | Low signal strength                        |
|  1 blue |  -120 to -110   | Very low signal strength                   |
|  1 red  |                 | Network unavailable (out of range)         |
|  2 red  |                 | Network access restricted (check subscription) |
|  4 red  |                 | LoRaWAN telemetry disabled. |


---
*Copyright 2019-2020, Codepoint Technologies,* 
*All Rights Reserved*
