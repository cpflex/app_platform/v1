/**
 *  Name:  TelemMgr.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

/**
* @brief Initializes Telemetry management functions.
*/
forward TelemMgr_Init();

/**
* @brief provides a visual indication of network status.
*/
forward bool: TelemMgr_IndicateNetworkStatus();