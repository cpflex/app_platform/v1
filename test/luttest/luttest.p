/**
 *  Name:  luttest.p
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */


#include "system.i"
#include "timer.i"
#include "log.i"
#include "location.i"
#include "ui.i"
#include "telemetry.i"


/*******************************************************
* Forward Declarations.
*******************************************************/
forward OnAlarm( tick, epoch);
forward OnAcquireWifi( epoch, ctMeas, Measurements: measurements);


public OnAlarm( tick, epoch)
{
	TraceDebug("2-long: Alarm Event");	
}

TestAlarm()
{
	TraceDebug("2-long: Test Alarm Begin");
	//Create an alarm every 10 seconds for 2 times.
	new tnow = Now();
	SetRtcAlarm( tnow + 10, 10, 2, "OnAlarm");
}

public OnAcquireWifi(epoch, ctMeas, Measurements: measurements)
{
	TraceDebug("1-long: Acq Complete.");
}

stock TestTelemSend()
{
	TraceDebug("2-double: Send Message Sequence");	
	new Sequence:seq;
	new ResultCode: rc;
	if( TelemSendSeq_Begin( seq, 130) == RC_success)
	{
		//Send an Int32 Message
		rc = TelemSendInt32Msg( seq, 12, 22 );

		//Send a KeyValueMsg
		if( rc == RC_success)
		{
			//new raw[] = [1,2,3,4,5,6];
			TelemSendKeyValueMsg_Begin( seq, 11, MP_high, MC_alert);
			TelemSendKeyValueMsg_PutString( seq, 0, "A Packed String");
			TelemSendKeyValueMsg_PutString( seq, 1, ''An unpacked string'');
			TelemSendKeyValueMsg_PutInt( seq, 2,22);
			//TelemSendEventPutRaw( seq, 3, raw, sizeof(raw));
			rc = TelemSendKeyValueMsg_End(seq);

			//Send an UnencodedMsg.
			if( rc == RC_success)
			{
				//new raw2[]= [10,9,8,7,6,5];
				TelemSendUnencodedMsg_Begin(seq, 10);
				//TelemSendUserPutRaw( seq, raw2, sizeof(raw2));
				TelemSendUnencodedMsg_PutString(seq, "A Packed String");
				TelemSendUnencodedMsg_PutJson( seq, "{\"field\": \"val\"}");
				rc = TelemSendUnencodedMsg_End(seq);
			}

		}
	}

	if( rc == RC_success)
	{
		TraceDebug("Sending");
	}
	else
	{
		TraceDebug("err- send seq construction" );
	}

	TelemSendSeq_End( seq, rc != RC_success );		
}

/*******************************************************
* System events.
*******************************************************/

@SysInit()
{
	TraceDebug("\r\n** LUT Test \r\n** Date: 2019/8/19");
	TraceDebug("* 1-short  Btn1 Test (Green LED 0)");
	TraceDebug("* 1-long   WiFi Acq");
	TraceDebug("* 2-short  Btn2 Test (Red LED 1)");
	TraceDebug("* 2-long   Alarm Test");
	TraceDebug("* 2-double Send Message");
}

/**
* UI Button Event Handler called whenever a button is pressed.
*/
@UiButtonEvent( idButton, ButtonPress: press, ctClicks)
{
	new count =  int:press + int:1;
	new bDefault = false;

	switch(idButton)
	{
		case BTN1: 
		{
			switch(press)
			{
				case BP_short: 	
				{	
					bDefault = true;
					TraceDebug("1-short");
				}
				case BP_long:  		
				{
					TraceDebug("1-long: wifi acq begin");
					AcquireMeasurements("OnAcquireWifi", PM_coarse, PT_wifi);
				}
				case BP_double:  	bDefault = true;
				case BP_verylong:  	bDefault = true;
				case BP_multi:      { count = ctClicks; bDefault = true;}
			}
		}
		case BTN2:
		{
			switch(press)
			{
				case BP_short: 		bDefault = true;
				case BP_long:		TestAlarm();
				case BP_double:  	TestTelemSend();
				case BP_verylong:  	bDefault = true;
				case BP_multi:      { count = ctClicks; bDefault = true;}
			}

		}
		case BTN_BOTH:
		{
			bDefault = true;
		}
	}

	if( bDefault)
	{	
		TraceDebugFmt( "Button Press: id=%d, press=%d, clicks=%d", idButton, int:press, ctClicks);
		UiSetLed( idButton, LM_count, count, 1000, 500 );
	}
}


@SysWake()
{
	//TraceDebug("SysWake Handled");
}

@SysSleep()
{
	//TraceDebug("SysSleep Handled");
}


