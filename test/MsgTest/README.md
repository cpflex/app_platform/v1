# CP-Flex Archive Test #

This application performs basic unit tests on the CP-Flex data store archive subsystem.
This is for development board testing only.

## User Interface Description ## 

The test application sends some of the non-standard messages

### Button Commands
Normal operating mode is entered when the device boots.

**Operating Mode Button Descriptions**
 
| Button 1 | Button 2 | Description |
|---|---|---|
| 1 quick press |  |  Initiates: <br/> A) Next Step (Step Mode), <br/>B) Re-runs all Tests (Timer Mode) |
| 2 quick presses|  |   Sets Step Mode |
| 3 quick presses|	|   Sets Timer Mode |
| 5 quick presses|	|   Clears the execution lock if it got stuck.|
| >10 sec. press | | Resets the device. |
| | N quick presses | Execute test #N.  (only works in step mode)|

**Operating Mode Button Descriptions** 

| LED #1 | LED #2 | Description |
|---|---|---|
| Blue blink three times | Blue blink three times | Device restarted |
| Blue blink once| Blue blink once| Test Started|
| Red blink three times | | Input invalid. |
| Blue blink twice | | Step Mode activated.  |
| Blue blink three times | | Timer Mode activated. |
| | Blue blink N times| Specifies the test running |

---
*Copyright 2021, Codepoint Technologies,* 
*All Rights Reserved*
