ECHO off
set lib=../../lib

set _DATE=%date%
set _TIME=%time%

REM Display Header.
ECHO Compiling Ped Tracker Application: 
ECHO Version: %_VERSION%
ECHO Date: %_DATE%
ECHO Time: %_TIME%

REM Create Version File with string data.
ECHO stock const __DATE__{} = "%_DATE%"; >		 version.i
ECHO stock const __TIME__{} = "%_TIME%"; >>		 version.i

pawncc ^
	%lib%/NvmRecTools.p timeadjust_test.p ^
	-Dsrc -S256 -X32768 -XD4092 ^
	-i. -i../../include -i%lib%  ^
	-otimeadjust_test.bin 	

