# CP-Flex Archive Test #

This application performs basic unit tests on the CP-Flex data store archive subsystem.
This is for development board testing only.

## User Interface Description ## 

The test application has two modes:

1. **Timer Mode** -- Timer automatically executes all tests in sequence.
2. **Step Mode** -- User can Step through the test sequence or execute selected tests (see button comands below).
 
Test Sequence is as follows:


| Test #  |  Description                  |
|---------|------------------------------|
| 1       |	TestEraseArchive() |
| 2       |	TestSysLog() |
| 3       |	TestPushString() |
| 4       |	TestStats(ART_ALL) |
| 5       |	TestPeekPop(false, ART_ALL) |
| 6       |	TestPushAppAndSysLogs() |
| 7       |	TestStats(ART_ALL) |
| 8       |	TestPeekPop(false, ART_ALL) |
| 9       |	TestPeekPop(true, ART_ALL) |
| 10      |	TestClear() |




### Button Commands
Normal operating mode is entered when the device boots.

**Operating Mode Button Descriptions**
 
| Button 1 | Button 2 | Description |
|---|---|---|
| 1 quick press |  |  Initiates: <br/> A) Next Step (Step Mode), <br/>B) Re-runs all Tests (Timer Mode) |
| 2 quick presses|  |   Sets Step Mode |
| 3 quick presses|	|   Sets Timer Mode |
| 5 quick presses|	|   Clears the execution lock if it got stuck.|
| >10 sec. press | | Resets the device. |
| | N quick presses | Execute test #N.  (only works in step mode)|

**Operating Mode Button Descriptions** 

| LED #1 | LED #2 | Description |
|---|---|---|
| Blue blink three times | Blue blink three times | Device restarted |
| Blue blink once| Blue blink once| Test Started|
| Red blink three times | | Input invalid. |
| Blue blink twice | | Step Mode activated.  |
| Blue blink three times | | Timer Mode activated. |
| | Blue blink N times| Specifies the test running |

---
*Copyright 2021, Codepoint Technologies,* 
*All Rights Reserved*
