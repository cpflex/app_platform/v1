# Nali - N100 LoRa Tag Performance Test #

The Flex application provides functions for testing the various performance aspects of the
Nali N100.  

## Instructions ##
1. Follow the instructions [**here**](../../../firmware/OTA/README.md) to install the perf_test.amx.bin file.
2. The perf test source code is in [perf_test.p](perf_test.p), it can be modified to suit your additional needs.

## Performance Test User Interface Description ## 
 
### Normal Operating Mode 
Normal operating mode is entered when the device boots.

**Operating Mode Button Descriptions**
 
| Button 1 | Button 2 | Description |
|---|---|---|
| 1 quick press or 3 to 6 presses|  |  Sends either location message or telemetry depending upon mode. |
| 2 quick press|  |  Indicates battery level (see Battery indicator below) |
| 1 long press (> 1.5 sec.)|	| Activates / Deactivates Auto Send (max 10,000 cycles). |
| 1 very long press |  |  Toggles location configuration mode (see Location Configuration Mode below).
|  | 1,2,3, ... quick presses | Auto Interval: 30 sec. (1), 1 min (2), 2 min (3), 5 min(4), 20 min (5), 30 min (6), 1 hr (7), 2 hr (8), 10 hr (9). The default is 5 minutes (4 clicks). |
|  |1 long press (>1.5 sec.)| Toggles data mode between sending location data (default) and test messages. |
|  |1 very long press (> 5 seconds) | Reads temperature |
| >15 sec. press | | Resets the device. |

**Operating Mode Button Descriptions** 

| LED #1 | LED #2 | Description |
|---|---|---|
| Blue blink three times | Blue blink three times | Device restarted |
| Purple blink five(5) times | Purple blink five(5) times | Enter location configuration mode (see below)|
| Red blink three times | | Input invalid. |
| Blue blink twice | | Auto send activated.  |
| Red blink twice | | Auto send deactivated. |
| Blue blink once | | Send location report.  |
| Orange blink N times| | Send N test messages |
| | Blue blink twice | Location data reporting mode set |
| | Purple blink twice | Test message reporting mode set |
| | Purple slow blink every 10 sec. | Battery is charging. |
| | Blue slow blink every 10 sec.  | Battery is charged. |
| | Four(4) red blinks every two minutes | Battery is nearly dead, place tag on charger |


**Battery Indicator**

Quickly pressing Button #1 twice will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2  | % Charge    | Description|
|---------|-------------|------------|
|  1 red  |   < 5%      | Battery level is critical,  charge immediately. |
|  1 blue |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 blue |  30% - 50%  | Battery is about 40% charged. |
|  3 blue |  50% - 70%  | Battery is about 60% charged. |
|  4 blue |  70% - 90%  | Battery  is about 80% charged. |
|  5 blue |  > 90%      | Battery is fully charged. |


### Location Configuration Mode  
Enables specification of the location measurement reporting mode.  Device
can be configured to test various performance and measurement modes.  To enter location 
configuration mode, press button #1 for longer than 5 seconds.  When released, the LEDs should 
blink purple 5 times.

**Location Configuration Mode Button Descriptions**
 
| Button 1 | Button 2 | Description |
|---|---|---|
| 1 long press |  |  Exit location configuration mode (see leds for feedback).
| 1 quick press|  |  Default performance mode (coarse for best battery life) |
| 2 quick press|  |  Coarse performance mode |
| 3 quick press|  |  Medium performance mode |
| 4 quick press|  |  Best performance mode   |
|   | 1 long presss | Indicate location mode (performance first, then tech)
|   | 1 quick press | Default measurement mode (WiFi and BLE) |
|   | 2 quick press | WiFi only measurement mode |
|   | 3 quick press | BLE only measurement mode|
|   | 4 quick press | WIFI and BLE measurement mode |

**Location Configuration Mode LED Descriptions** 

| LED #1 | LED #2 | Description |
|---|---|---|
| Blue blink five(5) times | Blue blink five(5) times | Exit location configuration mode|
| N blue blinks| | Indicates performance mode: 1-default, ... 4-best |
| | N blue blinks| Indicates measurement mode: 1-default, ... 4-WiFi/BLE |
| Red blink three times | | Input invalid. |



---
*Copyright 2019-2020, Codepoint Technologies,* 
*All Rights Reserved*
