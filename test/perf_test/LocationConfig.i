/**
 *  Name:  LocationConfig.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
 #include "ui.i"
 #include "location.i"

forward  bool: loccfg_OnButtonEvent(idButton, ButtonPress: press, ctClicks);
forward  PositioningMode:		  loccfg_getPosMode();
forward  PositioningTechnologies: loccfg_getPosTech();
forward  bool: loccfg_IsConfigModeActive();
forward  bool: loccfg_ToggleConfigMode();
forward  loccfg_IndicateConfiguration();
