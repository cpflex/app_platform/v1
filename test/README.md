# CP-Flex Test Applications 
This folder contains example test applications uese to create and validate the CP-Flex API and platform. 

The provided applications are use for testing, demonstration, and tutorial.  Use
one of these applications as the base for your own script development. They are 
described briefly as follows:

* [**perf_test**](apps/perf_test/README.md) -- Performance test for testing LoRa transmit and other wireless functions.
* [**apitest**](apitest/apitest.p) -- Basic test script used to validate the essential capabilities of the Flex API.
* [**empty**](Empty/Empty.p)   -- Empty project with no functionality provided as a template for new development.
* [**HelloWorld**](HelloWorld/HelloWorld.p) -- Basic project writes "Hello World" to the debug console.
* [**luttest**](luttest/luttest.p) -- Basic functional testing of the LoRa tag. 

To build the byte code make sure pawncc is in your path and execute either compile.sh or compile.bat depending on your system (either linux or Windows).

---
*Copyright 2019-2021, Codepoint Technologies,* 
*All Rights Reserved*
